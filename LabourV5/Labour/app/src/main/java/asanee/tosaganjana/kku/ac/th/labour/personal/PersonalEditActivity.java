package asanee.tosaganjana.kku.ac.th.labour.personal;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;


import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

import asanee.tosaganjana.kku.ac.th.labour.R;
import asanee.tosaganjana.kku.ac.th.labour.main.BaseActivity;
import asanee.tosaganjana.kku.ac.th.labour.main.MainActivity;
import asanee.tosaganjana.kku.ac.th.labour.main.date.DatePickerFragment;
import asanee.tosaganjana.kku.ac.th.labour.main.signup.ReadFileJSON;

import static android.content.ContentValues.TAG;

public class PersonalEditActivity extends PersonalActivity
        implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private static final int PICK_PHOTO_FOR_AVATAR = 1000;
    ImageView imageView;
    private Spinner spinnerProvince, spinnerDistinct, spinnerSubDistinct;
    private ArrayList<String> mProvince = new ArrayList<String>();
    private ArrayList<String> mDistinct = new ArrayList<String>();
    private ArrayList<String> mSubDistinct = new ArrayList<String>();

    private String changwats, amphoes, tambons;
    private String tmpProvice, tmpDistinct, tmpSubDistinct;
    private String changwatsPid, amphoesPid;
    private final static String SELECT = "-- pleases select --";
    private Context mContext;
    private Activity activity;
    private String id, name, lastName, tel, birthDate;
    private String gestational, pregnancy, sickness, congenital;
    private ReadFileJSON readFileJSON;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_edit);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle("แก้ไขข้อมูลส่วนตัว");


        init();
        initDrawerLayout(null);
        upDateUI();
        spinnerUpdate();
    }

    private void spinnerUpdate() {

        spinnerProvince = (Spinner) findViewById(R.id.thai_province);
        spinnerDistinct = (Spinner) findViewById(R.id.thai_distinct);
        spinnerSubDistinct = (Spinner) findViewById(R.id.thai_sub_distinct);

        spinnerProvince.setOnItemSelectedListener(this);
        spinnerDistinct.setOnItemSelectedListener(this);
        spinnerSubDistinct.setOnItemSelectedListener(this);

        readFileJSON = new ReadFileJSON(this);

        mProvince.clear();
        changwatsPid = readFileJSON.getProvince(SELECT, mProvince);

        readFileJSON.sort(mProvince);
        ArrayAdapter<String> adapterProvince = new ArrayAdapter<String>(mContext,
                android.R.layout.simple_dropdown_item_1line, mProvince);
        spinnerProvince.setAdapter(adapterProvince);

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        switch (parent.getId()) {
            case R.id.thai_province:
                changwats = mProvince.get(position);
                mProvince.clear();
                changwatsPid = readFileJSON.getProvince(changwats, mProvince);
                readFileJSON.sort(mProvince);
                mDistinct.clear();
                amphoesPid = readFileJSON.getDistinct(null, mDistinct, changwatsPid);
                readFileJSON.sort(mDistinct);
                ArrayAdapter<String> adapterDistinct = new ArrayAdapter<String>(mContext,
                        android.R.layout.simple_dropdown_item_1line, mDistinct);
                spinnerDistinct.setAdapter(adapterDistinct);
                break;
            case R.id.thai_distinct:
                amphoes = mDistinct.get(position);
                mDistinct.clear();
                amphoesPid = readFileJSON.getDistinct(amphoes, mDistinct, changwatsPid);
                readFileJSON.sort(mDistinct);
                mSubDistinct.clear();
                readFileJSON.getSubDistinct(mSubDistinct, amphoesPid);

                readFileJSON.sort(mSubDistinct);
                ArrayAdapter<String> adapterThai = new ArrayAdapter<String>(PersonalEditActivity.this,
                        android.R.layout.simple_dropdown_item_1line, mSubDistinct);
                spinnerSubDistinct.setAdapter(adapterThai);
                break;
            case R.id.thai_sub_distinct:
                tambons = mSubDistinct.get(position);
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.bgheader:
                pickImage();
                break;
            case R.id.fab_done:
                done();
                break;
            case R.id.btn_edit_date:
                datePicker();
                break;
            case R.id.btn_edit_address:
                editAddress();
                break;
            case R.id.btn_edit_address_cancel:
                cancelAddress();
                break;
            case R.id.back:
                back();
                break;
        }
    }

    private void init() {

        findViewById(R.id.btn_edit_date).setOnClickListener(this);
        findViewById(R.id.btn_edit_address).setOnClickListener(this);
        findViewById(R.id.btn_edit_address_cancel).setOnClickListener(this);
        findViewById(R.id.back).setOnClickListener(this);
        findViewById(R.id.fab_done).setOnClickListener(this);

        imageView = (ImageView) findViewById(R.id.bgheader);
        imageView.setOnClickListener(this);

        mContext = this;
        activity = this;
    }

    @Override
    public void back() {
        goBack(PersonalActivity.class);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home) {
            Intent intent = new Intent(getApplicationContext(), PersonalActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    private void done() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setMessage("You want to edit profile.");
        alert.setCancelable(false);
        alert.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                saveEditDone();

            }
        });
        alert.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alert.show();
    }

    private void editAddress() {
        LinearLayout addressLay = (LinearLayout) findViewById(R.id.address);
        LinearLayout editAddressLay = (LinearLayout) findViewById(R.id.edit_address);

        addressLay.setVisibility(View.GONE);
        editAddressLay.setVisibility(View.VISIBLE);

        tmpProvice = changwats;
        tmpDistinct = amphoes;
        tmpSubDistinct = tambons;
    }

    private void cancelAddress() {
        LinearLayout addressLay = (LinearLayout) findViewById(R.id.address);
        LinearLayout editAddressLay = (LinearLayout) findViewById(R.id.edit_address);

        addressLay.setVisibility(View.VISIBLE);
        editAddressLay.setVisibility(View.GONE);

        changwats = tmpProvice;
        amphoes = tmpDistinct;
        tambons = tmpSubDistinct;
    }

    public void pickImage() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, PICK_PHOTO_FOR_AVATAR);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_PHOTO_FOR_AVATAR && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                return;
            }

            try {

                InputStream inputStream = getContentResolver().openInputStream(data.getData());
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);

                Drawable drawable = new BitmapDrawable(getResources(), bitmap);
                Log.d(TAG, String.valueOf(inputStream));


                imageView.setImageDrawable(drawable);


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private void saveEditDone() {
        if (isValidate()) {
            showProgressDialog();

            String UID = FirebaseAuth.getInstance().getCurrentUser().getUid();
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            final DatabaseReference myRef = database.getReference(UID).child("information");

            myRef.child("name").setValue(name);
            myRef.child("lastname").setValue(lastName);
            myRef.child("birthdate").setValue(birthDate);
            myRef.child("tel").setValue(tel);
            myRef.child("address").child("province").setValue(changwats);
            myRef.child("address").child("distinct").setValue(amphoes);
            myRef.child("address").child("sub distinct").setValue(tambons);
            myRef.child("id").setValue(id);
            myRef.child("sickness").setValue(sickness);
            myRef.child("congenital").setValue(congenital);
            myRef.child("pregnancy").setValue(pregnancy);
            myRef.child("gestational").setValue(gestational);

            StorageReference mStorageRef = FirebaseStorage.getInstance().getReference();
            StorageReference imageRef = mStorageRef.child(UID + "/profile.JPEG");

            imageView.buildDrawingCache();
            Bitmap bitmap = imageView.getDrawingCache();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] task = baos.toByteArray();

            UploadTask mUploadTask = imageRef.putBytes(task);


            imageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    String url = uri.toString();
                    myRef.child("image").setValue(url);
                    Log.d("url", url);

                    Intent intent = new Intent(getApplicationContext(), PersonalActivity.class);
//                    intent.putExtra("url", url);
                    startActivity(intent);
                    hideProgressDialog();
                }
            });


        }
    }

    private boolean isValidate() {

        final EditText edtId = (EditText) findViewById(R.id.edt_id);
        final EditText edtName = (EditText) findViewById(R.id.edt_name);
        final EditText edtLastName = (EditText) findViewById(R.id.edt_lastname);
        final EditText edtBirthdate = (EditText) findViewById(R.id.edt_date);
        final EditText edtTel = (EditText) findViewById(R.id.edt_tel);
        final EditText edtSickness = (EditText) findViewById(R.id.edt_sickness);
        final EditText edtCongenital = (EditText) findViewById(R.id.edt_congenital_disorder);
        final EditText edtGestational = (EditText) findViewById(R.id.edt_gestational_age);
        final EditText edtPregnancy = (EditText) findViewById(R.id.edt_pregnancy);

        id = edtId.getText().toString();
        name = edtName.getText().toString();
        lastName = edtLastName.getText().toString();
        birthDate = edtBirthdate.getText().toString();
        tel = edtTel.getText().toString();
        sickness = edtSickness.getText().toString();
        congenital = edtCongenital.getText().toString();
        gestational = edtGestational.getText().toString();
        pregnancy = edtPregnancy.getText().toString();

        if (id.length() != 13) {
            Toast.makeText(getApplicationContext(), "Your Id is Invalid.",
                    Toast.LENGTH_SHORT).show();
            return false;
        } else if (name.isEmpty() ||
                lastName.isEmpty() ||
                tel.isEmpty() ||
                birthDate.isEmpty()) {

            Toast.makeText(getApplicationContext(), "All fields are required.",
                    Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void datePicker() {
        DialogFragment newFragment = new DatePickerFragment(getApplicationContext(), activity);
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    private void upDateUI() {

        showProgressDialog();

        String UID = FirebaseAuth.getInstance().getCurrentUser().getUid();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(UID).child("information");

        final EditText edtId = (EditText) findViewById(R.id.edt_id);
        final EditText edtName = (EditText) findViewById(R.id.edt_name);
        final EditText edtLastName = (EditText) findViewById(R.id.edt_lastname);
        final EditText edtBirthdate = (EditText) findViewById(R.id.edt_date);
        final EditText edtTel = (EditText) findViewById(R.id.edt_tel);
        final EditText edtAddress = (EditText) findViewById(R.id.edt_address);
        final EditText edtSickness = (EditText) findViewById(R.id.edt_sickness);
        final EditText edtCongenital = (EditText) findViewById(R.id.edt_congenital_disorder);
        final EditText edtGestational = (EditText) findViewById(R.id.edt_gestational_age);
        final EditText edtPregnancy = (EditText) findViewById(R.id.edt_pregnancy);
        final EditText edtEmail = (EditText) findViewById(R.id.edt_email);
        final EditText edtLine = (EditText) findViewById(R.id.edt_line);
        final EditText edtBirthRecord = (EditText) findViewById(R.id.edt_birth_record);

        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String setId = dataSnapshot.child("id").getValue(String.class);
                String setName = dataSnapshot.child("name").getValue(String.class);
                String setLastName = dataSnapshot.child("lastname").getValue(String.class);
                String setProvince = dataSnapshot.child("address").child("province").getValue(String.class);
                String setDistinct = dataSnapshot.child("address").child("distinct").getValue(String.class);
                String setSubDistinct = dataSnapshot.child("address").child("sub distinct").getValue(String.class);
                String setBirthDate = dataSnapshot.child("birthdate").getValue(String.class);
                String setTel = dataSnapshot.child("tel").getValue(String.class);
                String setPregnancy = dataSnapshot.child("pregnancy").getValue(String.class);
                String setSickness = dataSnapshot.child("sickness").getValue(String.class);
                String setGestational = dataSnapshot.child("gestational").getValue(String.class);
                String setCongenital = dataSnapshot.child("congenital").getValue(String.class);
                String setEmail = dataSnapshot.child("email").getValue(String.class);
                String setLine = dataSnapshot.child("line").getValue(String.class);
                String setBirthRecord = dataSnapshot.child("birthRecord").getValue(String.class);
                String uri = dataSnapshot.child("image").getValue(String.class);

                if (uri != null) {

                    Glide.with(getApplicationContext())
                            .load(uri)
                            .fitCenter()
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(imageView);
                }

                edtId.setText(setId);
                edtName.setText(setName);
                edtLastName.setText(setLastName);
                edtBirthdate.setText(setBirthDate);
                edtTel.setText(setTel);
                edtAddress.setText("ต." + setSubDistinct + " อ." + setDistinct + " จ." + setProvince);
                edtCongenital.setText(setCongenital);
                edtGestational.setText(setGestational);
                edtPregnancy.setText(setPregnancy);
                edtSickness.setText(setSickness);
                edtBirthRecord.setText(setBirthRecord);
                edtEmail.setText(setEmail);
                edtLine.setText(setLine);

                changwats = setProvince;
                amphoes = setDistinct;
                tambons = setSubDistinct;

                id = edtId.getText().toString();
                name = edtName.getText().toString();
                lastName = edtLastName.getText().toString();
                birthDate = edtBirthdate.getText().toString();
                tel = edtTel.getText().toString();

                hideProgressDialog();
            }

            @Override
            public void onCancelled(DatabaseError error) {
            }
        });

    }

}
