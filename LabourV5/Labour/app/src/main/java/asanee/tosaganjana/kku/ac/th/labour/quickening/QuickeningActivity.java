package asanee.tosaganjana.kku.ac.th.labour.quickening;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.widget.TextView;

import asanee.tosaganjana.kku.ac.th.labour.R;
import asanee.tosaganjana.kku.ac.th.labour.main.BaseActivity;
import asanee.tosaganjana.kku.ac.th.labour.main.MainActivity;

public class QuickeningActivity extends BaseActivity {

    private ViewPager tabsviewPager;
    private Tabsadapter mTabsAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quickening);

        setTitle(getString(R.string.labour_quickening));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(false);

        TextView tv1 = (TextView) findViewById(R.id.quickening_1);
        TextView tv2 = (TextView) findViewById(R.id.quickening_2);
        TextView tv3 = (TextView) findViewById(R.id.quickening_3);
        TextView tv4 = (TextView) findViewById(R.id.quickening_4);

        tv1.setText("ลูกดิ้น >3 ครั้ง");
        tv2.setText("ลูกดิ้น <3 ครั้ง");
        tv3.setText("ลูกดิ้น <3 ครั้ง");
        tv4.setText("ลูกดิ้น <3 ครั้ง");


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
           goBack(MainActivity.class);
        }

        return super.onOptionsItemSelected(item);
    }

}
