package asanee.tosaganjana.kku.ac.th.labour.test;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.Scanner;

import asanee.tosaganjana.kku.ac.th.labour.R;
import asanee.tosaganjana.kku.ac.th.labour.fonts.CustomButton;
import asanee.tosaganjana.kku.ac.th.labour.fonts.CustomRadioButton;
import asanee.tosaganjana.kku.ac.th.labour.fonts.CustomTextView;
import asanee.tosaganjana.kku.ac.th.labour.main.BaseActivity;


public class PreTestActivity extends BaseActivity implements View.OnClickListener {

    private StringBuilder builder;
    private Button submit, next;
    private TextView problem, text_part;
    private int score = 0, count = 0;
    private RadioGroup group;
    private RadioButton radio1, radio2;
    private JSONArray exam;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_test);

        init();
        builder();


    }

    private void builder() {
        Resources res = getResources();
        InputStream is = res.openRawResource(R.raw.pretest);
        Scanner scanner = new Scanner(is);

        builder = new StringBuilder();

        while (scanner.hasNextLine()) {
            builder.append(scanner.nextLine());
        }
        readFile(builder.toString(), count);
    }

    private void init() {

        problem = (CustomTextView) findViewById(R.id.problem);
        text_part = (CustomTextView) findViewById(R.id.part);
        group = (RadioGroup) findViewById(R.id.radio_group);
        radio1 = (CustomRadioButton) findViewById(R.id.radio1);
        radio2 = (CustomRadioButton) findViewById(R.id.radio2);

        submit = (CustomButton) findViewById(R.id.submit);
        next = (CustomButton) findViewById(R.id.next);

        next.setOnClickListener(this);
        submit.setOnClickListener(this);

    }

    public void readFile(String s, int i) {
        try {
            submit.setVisibility(View.INVISIBLE);
            group.clearCheck();
            JSONObject root = new JSONObject(s);
            exam = (JSONArray) root.get("preTest");

            String question = exam.getJSONObject(i).getString("problem" + String.valueOf(i + 1));
            String choice1 = exam.getJSONObject(i).getString("choice1");
            String choice2 = exam.getJSONObject(i).getString("choice2");
            String part = exam.getJSONObject(i).getString("part");

            text_part.setText(part);
            problem.setText(question);
            radio1.setText("A. " + choice1);
            radio2.setText("B. " + choice2);

        } catch (JSONException e) {

        }

    }

    //num 1 - 4
    public void checkAns(int i) {
        if (i == 1) {
            if (count + 1 == exam.length()) {
                radio1.setChecked(true);
                radio2.setEnabled(false);
            }
            score += 1;
        } else if (i == 2) {
            if (count + 1 == exam.length()) {
                radio2.setChecked(true);
                radio1.setEnabled(false);
            }
            score += 0;
        }
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.submit:
                goTo(ResultPreTest.class, "score", score);
                break;
            case R.id.next:
                next();
                break;
        }
    }

    private void next() {
        if (radio1.isChecked() || radio2.isChecked()) {
            //check number 1 -4
            if (radio1.isChecked()) {
                checkAns(1);
            } else if (radio2.isChecked()) {
                checkAns(2);
            }
            count++;
            if (count == exam.length()) { // last number will show button submit and hide button next

                submit.setVisibility(View.VISIBLE);
                next.setVisibility(View.INVISIBLE);
            } else {
                readFile(builder.toString(), count);
            }

        } else
            Toast.makeText(PreTestActivity.this, "Please select answer", Toast.LENGTH_LONG).show();
    }
}
