package asanee.tosaganjana.kku.ac.th.labour.main;

/**
 * Created by Puppymind on 29/4/2560.
 */
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import asanee.tosaganjana.kku.ac.th.labour.R;

public class BaseActivity extends AppCompatActivity {
    private ProgressDialog mProgressDialog;

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("loading...");
            mProgressDialog.setIndeterminate(true);
        }
        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        hideProgressDialog();
    }

    public void goBack(Class mClass) {

        Intent intent = new Intent(this, mClass);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_righ);
    }

    public void goTo(Class mClass) {

        Intent intent = new Intent(this, mClass);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public void goTo(Class mClass,String key,String value) {

        Intent intent = new Intent(this, mClass);
        intent.putExtra(key,value);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public void goTo(Class mClass,String key,int value) {

        Intent intent = new Intent(this, mClass);
        intent.putExtra(key,value);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
}