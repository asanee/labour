package asanee.tosaganjana.kku.ac.th.labour.test;


import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.Scanner;

import asanee.tosaganjana.kku.ac.th.labour.R;
import asanee.tosaganjana.kku.ac.th.labour.fonts.CustomButton;
import asanee.tosaganjana.kku.ac.th.labour.fonts.CustomRadioButton;
import asanee.tosaganjana.kku.ac.th.labour.fonts.CustomTextView;
import asanee.tosaganjana.kku.ac.th.labour.main.BaseActivity;
import asanee.tosaganjana.kku.ac.th.labour.main.MainActivity;

public class TestActivity extends BaseActivity implements View.OnClickListener {

    private StringBuilder builder;
    private CustomButton submit, next;
    private CustomTextView problem;
    private int score = 0, count = 0;
    private String choice3;
    private RadioGroup group;
    private CustomRadioButton radio1, radio2, radio3;
    private JSONArray exam;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        init();
        builder();



    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.back:
                goBack(MainActivity.class);
                break;
            case R.id.submit:
                goTo(ResultTest.class, "score", score);
                break;
            case R.id.next:
                next();
                break;
        }
    }

    private void init() {
        problem = (CustomTextView) findViewById(R.id.problem);
        group = (RadioGroup) findViewById(R.id.radio_group);
        radio1 = (CustomRadioButton) findViewById(R.id.radio1);
        radio2 = (CustomRadioButton) findViewById(R.id.radio2);
        radio3 = (CustomRadioButton) findViewById(R.id.radio3);

        submit = (CustomButton) findViewById(R.id.submit);
        next = (CustomButton) findViewById(R.id.next);

        submit.setOnClickListener(this);
        next.setOnClickListener(this);

        findViewById(R.id.back).setOnClickListener(this);


    }

    private void builder() {
        Resources res = getResources();
        InputStream is = res.openRawResource(R.raw.test);
        Scanner scanner = new Scanner(is);

        builder = new StringBuilder();

        while (scanner.hasNextLine()) {
            builder.append(scanner.nextLine());
        }
        readFile(builder.toString(), count);
    }

    public void readFile(String s, int i) {

        try {
            group.clearCheck();
            JSONObject root = new JSONObject(s);
            exam = (JSONArray) root.get("exam");

            String question = exam.getJSONObject(i).getString("problem" + String.valueOf(i + 1));
            String choice1 = exam.getJSONObject(i).getString("choice1");
            String choice2 = exam.getJSONObject(i).getString("choice2");
            if (i <= 3) {
                submit.setVisibility(View.INVISIBLE);
                choice3 = exam.getJSONObject(i).getString("choice3");
                radio3.setText("C. " + choice3);
            } else {
                submit.setVisibility(View.INVISIBLE);
                radio3.setVisibility(View.INVISIBLE);
            }

            problem.setText(question);
            radio1.setText("A. " + choice1);
            radio2.setText("B. " + choice2);

        } catch (JSONException e) {

        }

    }

    private void next() {
        if (radio1.isChecked() || radio2.isChecked() || radio3.isChecked()) {
            if (count <= 3) {   //check number 1 -4
                if (radio1.isChecked()) {
                    checkAns1(1);
                } else if (radio2.isChecked()) {
                    checkAns1(2);
                } else {
                    checkAns1(3);
                }
                count++;
                readFile(builder.toString(), count);

            } else { // check number 4 - last num

                if (radio1.isChecked()) {
                    checkAns2(1);
                } else if (radio2.isChecked()) {
                    checkAns2(2);
                }

                count++;
                if (count == exam.length()) { // last number will show button submit and hide button next

                    submit.setVisibility(View.VISIBLE);
                    next.setVisibility(View.GONE);
                } else {
                    readFile(builder.toString(), count);
                }
            }
        } else
            Toast.makeText(TestActivity.this, "Please select answer",
                    Toast.LENGTH_LONG).show();
    }

    //num 1 - 4
    public void checkAns1(int i) {
        if (i == 1) {
            score += 10;
        } else if (i == 2) {
            score += 2;
        } else if (i == 3) {
            score += 0;
        }
    }

    //num 4 - last num
    public void checkAns2(int i) {

        if (i == 1) {
            if (count + 1 == exam.length()) {
                radio1.setChecked(true);
                radio2.setEnabled(false);
            }
            score += 10;

        } else if (i == 2) {
            if (count + 1 == exam.length()) {
                radio2.setChecked(true);
                radio1.setEnabled(false);
            }
            score += 0;
        }
    }


}