package asanee.tosaganjana.kku.ac.th.labour.personal;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.StringSignature;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;


import asanee.tosaganjana.kku.ac.th.labour.R;
import asanee.tosaganjana.kku.ac.th.labour.main.BaseActivity;
import asanee.tosaganjana.kku.ac.th.labour.main.MainActivity;
import asanee.tosaganjana.kku.ac.th.labour.personal.admin.AdminActivity;


public class PersonalActivity extends MainActivity implements View.OnClickListener {

    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle("ข้อมูลส่วนตัว");

        initDrawerLayout(null);
        init();
        upDateUI();
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.fab_edit:
                goTo(PersonalEditActivity.class);
                break;
            case R.id.back:
                back();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        back();
    }

    private void init() {

        findViewById(R.id.fab_edit).setOnClickListener(this);
        findViewById(R.id.back).setOnClickListener(this);
    }

    private void upDateUI() {

        showProgressDialog();

        final TextView tvId = (TextView) findViewById(R.id.personal_id);
        final TextView tvName = (TextView) findViewById(R.id.personal_name);
        final TextView tvLastName = (TextView) findViewById(R.id.personal_lastname);
        final TextView tvAge = (TextView) findViewById(R.id.personal_age);
        final TextView tvAddress = (TextView) findViewById(R.id.personal_address);
        final TextView tvTel = (TextView) findViewById(R.id.personal_tel);
        final TextView tvSickness = (TextView) findViewById(R.id.personal_sickness);
        final TextView tvCongenital = (TextView) findViewById(R.id.personal_congenital_disorder);
        final TextView tvGestational = (TextView) findViewById(R.id.personal_gestational_age);
        final TextView tvPregnancy = (TextView) findViewById(R.id.personal_pregnancy);
        final TextView tvPreTest = (TextView) findViewById(R.id.personal_pre_test);
        final TextView tvPostTest = (TextView) findViewById(R.id.personal_post_test);
        final TextView tvEmail = (TextView) findViewById(R.id.personal_email);
        final TextView tvLine = (TextView) findViewById(R.id.personal_line);
        final TextView tvBirthRecord = (TextView) findViewById(R.id.personal_birth_record);
        final ImageView imageView = (ImageView) findViewById(R.id.bgheader);
//        LinearLayout preTest = (LinearLayout) findViewById(R.id.pre_test);
//        LinearLayout postTest = (LinearLayout) findViewById(R.id.post_test);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_edit);

        String UID = FirebaseAuth.getInstance().getCurrentUser().getUid();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            UID = bundle.getString("key");
            fab.setVisibility(View.GONE);

        }else {
            fab.setVisibility(View.VISIBLE);
        }

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(UID).child("information");


        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String id = dataSnapshot.child("id").getValue(String.class);
                String name = dataSnapshot.child("name").getValue(String.class);
                String lastName = dataSnapshot.child("lastname").getValue(String.class);
                String province = dataSnapshot.child("address").child("province").getValue(String.class);
                String distinct = dataSnapshot.child("address").child("distinct").getValue(String.class);
                String subDistinct = dataSnapshot.child("address").child("sub distinct").getValue(String.class);
                String birthDate = dataSnapshot.child("birthdate").getValue(String.class);
                String tel = dataSnapshot.child("tel").getValue(String.class);
                String email = dataSnapshot.child("email").getValue(String.class);
                String line = dataSnapshot.child("line").getValue(String.class);
                String pregnancy = dataSnapshot.child("pregnancy").getValue(String.class);
                String sickness = dataSnapshot.child("sickness").getValue(String.class);
                String gestational = dataSnapshot.child("gestational").getValue(String.class);
                String congenital = dataSnapshot.child("congenital").getValue(String.class);
                String birthRecord = dataSnapshot.child("birthRecord").getValue(String.class);
                String preTest = dataSnapshot.child("pre test").getValue(String.class);
                String postTest = dataSnapshot.child("post test").getValue(String.class);
                url = dataSnapshot.child("image").getValue(String.class);

                if (url != null) {
                    Glide.with(PersonalActivity.this)
                            .load(url)
                            .centerCrop()
                            .signature(new StringSignature(UUID.randomUUID().toString()))
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .into(imageView);
                }

                if(preTest == null){
                    tvPreTest.setText(": ยังไม่ได้ทำแบบประเมิน");
                }else {
                    tvPreTest.setText(": " + preTest);
                }

                if(postTest == null){
                    tvPostTest.setText(": ยังไม่ได้ทำแบบประเมิน");
                }else {
                    tvPostTest.setText(": " + preTest);
                }

                tvId.setText(": " + id);
                tvName.setText(": " + name);
                tvLastName.setText(": " + lastName);
                tvAddress.setText(": " + "ต." + subDistinct + " อ." + distinct + " จ." + province);
                tvTel.setText(": " + tel);
                tvAge.setText(": " + getAge(birthDate));
                tvSickness.setText(": " + sickness);
                tvCongenital.setText(": " + congenital);
                tvPregnancy.setText(": " + pregnancy);
                tvGestational.setText(": " + gestational);
                tvEmail.setText(": " + email);
                tvLine.setText(": " + line);
                tvBirthRecord.setText(": " + birthRecord);

                hideProgressDialog();
            }

            @Override
            public void onCancelled(DatabaseError error) {


            }
        });

    }

    private String getAge(String birthdate) {
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        Date date = null;
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");

        try {
            date = format.parse(birthdate);

        } catch (Exception e) {
            e.printStackTrace();
        }

        dob.setTime(date);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }

        Integer ageInt = new Integer(age);
        String ageS = ageInt.toString();

        return ageS;
    }

    public void back() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            goBack(AdminActivity.class);
        } else {
            goBack(MainActivity.class);
        }
    }


    public void goBack(Class mClass) {

        Intent intent = new Intent(this, mClass);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_righ);
    }


}
