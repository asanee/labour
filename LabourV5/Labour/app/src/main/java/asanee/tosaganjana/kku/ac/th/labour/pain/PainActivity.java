package asanee.tosaganjana.kku.ac.th.labour.pain;


import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import asanee.tosaganjana.kku.ac.th.labour.R;
import asanee.tosaganjana.kku.ac.th.labour.main.BaseActivity;
import asanee.tosaganjana.kku.ac.th.labour.main.MainActivity;

public class PainActivity extends BaseActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pain);

        setTitle(getString(R.string.labour_pain));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Button btn1 = (Button) findViewById(R.id.btn_pain_1);
        Button btn2 = (Button) findViewById(R.id.btn_pain_2);

        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);



    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            goBack(MainActivity.class);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_pain_1:
                goTo(PainIndexActivity_1.class);
                break;
            case R.id.btn_pain_2:
                goTo(PainIndexActivity_2.class);
                break;
        }
    }
}
