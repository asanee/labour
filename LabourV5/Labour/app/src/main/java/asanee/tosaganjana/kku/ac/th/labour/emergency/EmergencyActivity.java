package asanee.tosaganjana.kku.ac.th.labour.emergency;

import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import asanee.tosaganjana.kku.ac.th.labour.R;
import asanee.tosaganjana.kku.ac.th.labour.main.BaseActivity;
import asanee.tosaganjana.kku.ac.th.labour.main.MainActivity;

public class EmergencyActivity extends BaseActivity implements View.OnClickListener{

    private ViewPager tabsviewPager;
    private Tabsadapter mTabsAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency);

        setTitle(getString(R.string.labour_emergency));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Button btn1 = (Button) findViewById(R.id.btn_emergency_1);
        Button btn2 = (Button) findViewById(R.id.btn_emergency_2);

        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            goBack(MainActivity.class);
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_emergency_1:
                goTo(EmergencyIndexActivity_1.class);
                break;
            case R.id.btn_emergency_2:
                goTo(EmergencyIndexActivity_2.class);
                break;

        }
    }


}
