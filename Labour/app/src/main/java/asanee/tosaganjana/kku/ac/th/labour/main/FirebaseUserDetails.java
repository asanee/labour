package asanee.tosaganjana.kku.ac.th.labour.main;

/**
 * Created by Puppymind on 29/4/2560.
 */

public class FirebaseUserDetails {

    private String name;
    private String lastname;
    private String birthdate;
    private String id;
    private String tel;
    private String address;
    private String gender;

    public FirebaseUserDetails() {

    }

    public FirebaseUserDetails(String name, String lastname, String birthdate,
                               String id, String tel, String address, String gender) {

        this.name = name;
        this.lastname = lastname;
        this.birthdate = birthdate;
        this.id = id;
        this.tel = tel;
        this.address = address;
        this.gender = gender;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

}