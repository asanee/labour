package asanee.tosaganjana.kku.ac.th.labour.list.index;

/**
 * Created by Puppymind on 29/4/2560.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import asanee.tosaganjana.kku.ac.th.labour.fonts.CustomTextView;
import asanee.tosaganjana.kku.ac.th.labour.R;

public class IndexAdapter extends RecyclerView.Adapter<IndexAdapter.ViewHolder> {

    private List<index> mIndices;
    private Context mContext;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public CustomTextView mTitle;
        public CustomTextView mAuthor;
        public TextView mIndex;

        public ViewHolder(View view) {
            super(view);

            mTitle = (CustomTextView) view.findViewById(R.id.title);
            mAuthor = (CustomTextView) view.findViewById(R.id.author);
            mIndex = (TextView) view.findViewById(R.id.index_text);
        }
    }

    public IndexAdapter(Context context, List<index> dataset) {
        mIndices = dataset;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.recycler_view_row, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        index index = mIndices.get(position);

        viewHolder.mIndex.setText(String.valueOf(index.getIndex()));
        viewHolder.mTitle.setText(index.getTitle());
        viewHolder.mAuthor.setText("โดย : " + index.getAuthor());
    }

    @Override
    public int getItemCount() {
        return mIndices.size();
    }
}