package asanee.tosaganjana.kku.ac.th.labour.main;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;

import asanee.tosaganjana.kku.ac.th.labour.R;
import asanee.tosaganjana.kku.ac.th.labour.emergency.EmergencyActivity;
import asanee.tosaganjana.kku.ac.th.labour.fonts.CustomTypefaceSpan;
import asanee.tosaganjana.kku.ac.th.labour.pain.PainActivity;
import asanee.tosaganjana.kku.ac.th.labour.personal.PersonalActivity;
import asanee.tosaganjana.kku.ac.th.labour.preparing.PreparingActivity;
import asanee.tosaganjana.kku.ac.th.labour.quickening.QuickeningActivity;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, GoogleApiClient.OnConnectionFailedListener {
    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle("Home".toUpperCase());

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        onNavigationView();

        MainFragment MainFragment = new MainFragment();
        FragmentManager manager = getSupportFragmentManager();
        manager.beginTransaction().replace(
                R.id.content_main_fragment,
                MainFragment,
                MainFragment.getTag()
        ).commit();

        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        Intent setIntent = new Intent(Intent.ACTION_MAIN);
        setIntent.addCategory(Intent.CATEGORY_HOME);
        setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(setIntent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.createFromAsset(getAssets(), "THSarabun.ttf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        String title = (String) mi.getTitle();
        mNewTitle.setSpan(new RelativeSizeSpan(1.5f), 0,title.length(), 0); // set size
        mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    private void onNavigationView() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        Menu nav_Menu = navigationView.getMenu();

        for (int i = 0; i < nav_Menu.size(); i++) {
            MenuItem mi = nav_Menu.getItem(i);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);

                }
            }

            //the method we have create in activity
            applyFontToMenuItem(mi);
        }

        String UID = null;
        try {
            UID = FirebaseAuth.getInstance().getCurrentUser().getUid();
        } catch (Exception e) {

        }

        if (UID == null) {
            nav_Menu.findItem(R.id.nav_personal).setVisible(false);
            nav_Menu.findItem(R.id.nav_signOut).setVisible(false);
            nav_Menu.findItem(R.id.nav_signIn).setVisible(true);

        } else {
            nav_Menu.findItem(R.id.nav_personal).setVisible(true);
            nav_Menu.findItem(R.id.nav_signOut).setVisible(true);
            nav_Menu.findItem(R.id.nav_signIn).setVisible(false);
        }


        navigationView.setNavigationItemSelectedListener(this);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
//        if (id == R.id.nav_home) {
//
//        }
        if (id == R.id.nav_labour_pain) {
            Intent intent = new Intent(this, PainActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_labour_emergency) {
            Intent intent = new Intent(this, EmergencyActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_labour_quickening) {
            Intent intent = new Intent(this, QuickeningActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_labour_preparing) {
            Intent intent = new Intent(this, PreparingActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_tools) {

        } else if (id == R.id.nav_signIn) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_signOut) {
            signOut();
        } else if (id == R.id.nav_personal) {
            Intent intent = new Intent(this, PersonalActivity.class);
            startActivity(intent);
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void signOut() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setMessage(R.string.logout);
        alert.setCancelable(false);
        alert.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // Firebase sign out
                FirebaseAuth.getInstance().signOut();
                finish();
                startActivity(getIntent());
                // Google sign out
                Auth.GoogleSignInApi.signOut(mGoogleApiClient);
            }
        });
        alert.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alert.show();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        String TAG = "MainActivity";
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
        Toast.makeText(this, "Google Play Services error.", Toast.LENGTH_SHORT).show();
    }
}
