package asanee.tosaganjana.kku.ac.th.labour.main;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import asanee.tosaganjana.kku.ac.th.labour.R;
import asanee.tosaganjana.kku.ac.th.labour.emergency.EmergencyActivity;
import asanee.tosaganjana.kku.ac.th.labour.pain.PainActivity;
import asanee.tosaganjana.kku.ac.th.labour.preparing.PreparingActivity;
import asanee.tosaganjana.kku.ac.th.labour.test.TestActivity;


/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment implements View.OnClickListener {


    public MainFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        RelativeLayout btnPain = (RelativeLayout) view.findViewById(R.id.labour_pain);
        FrameLayout btnTest = (FrameLayout) view.findViewById(R.id.btn_test);
        RelativeLayout btnEmergency = (RelativeLayout) view.findViewById(R.id.labour_emergency);
        RelativeLayout btnPreparing = (RelativeLayout) view.findViewById(R.id.labour_preparing);
        RelativeLayout btnQuickening = (RelativeLayout) view.findViewById(R.id.labour_quickening);

        btnTest.setOnClickListener(this);
        btnPain.setOnClickListener(this);
        btnEmergency.setOnClickListener(this);
        btnPreparing.setOnClickListener(this);
        btnQuickening.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.labour_pain:
                intent = new Intent(getActivity(), PainActivity.class);
                startActivity(intent);
                break;

            case R.id.btn_test:
                intent = new Intent(getActivity(), TestActivity.class);
                startActivity(intent);
                break;

            case R.id.labour_preparing:
                intent = new Intent(getActivity(), PreparingActivity.class);
                startActivity(intent);
                break;

            case R.id.labour_emergency:
                intent = new Intent(getActivity(), EmergencyActivity.class);
                startActivity(intent);
                break;

            case R.id.labour_quickening:
                intent = new Intent(getActivity(), PreparingActivity.class);
                startActivity(intent);
                break;
        }

    }
}
