package asanee.tosaganjana.kku.ac.th.labour.pain;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import asanee.tosaganjana.kku.ac.th.labour.R;
import asanee.tosaganjana.kku.ac.th.labour.list.index.IndexAdapter;
import asanee.tosaganjana.kku.ac.th.labour.list.index.RecyclerItemClickListener;
import asanee.tosaganjana.kku.ac.th.labour.list.index.index;

/**
 * A simple {@link Fragment} subclass.
 */
public class IndexPainFragment extends Fragment implements View.OnClickListener{

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    Thread splashTread;
    View view;

    public IndexPainFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_index_pain, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), mRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                // ...
                Toast.makeText(getActivity(), String.valueOf(position +1),Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onItemLongClick(View view, int position) {
                // ...
            }
        }));
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new IndexAdapter(getActivity(), initIndex());
        mRecyclerView.setAdapter(mAdapter);

        StartAnimations();
        return view;
    }

    private List<index> initIndex() {

        List<index> dataset = new ArrayList<index>();

        for (int i = 1; i <= 20; i++) {
            index item = new index(i, "Title : " + i, "Author " + i);
            dataset.add(item);
        }


        return dataset;
    }

    @Override
    public void onClick(final View view) {

        int itemPosition = mRecyclerView.getChildLayoutPosition(view);

        Toast.makeText(getActivity(), itemPosition+"", Toast.LENGTH_LONG).show();
    }

    private void StartAnimations() {
        Animation anim = AnimationUtils.loadAnimation(getActivity(), R.anim.alpha);
        anim.reset();
        RelativeLayout r =(RelativeLayout) view.findViewById(R.id.host);
        r.clearAnimation();
        r.startAnimation(anim);

        anim = AnimationUtils.loadAnimation(getActivity(), R.anim.translate);
        anim.reset();
        RecyclerView l =(RecyclerView) view.findViewById(R.id.recycler_view);
        l.clearAnimation();
        l.startAnimation(anim);

        splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    int waited = 0;
                    // Splash screen pause time
                    while (waited < 6000) {
                        sleep(100);
                        waited += 100;
                    }


                } catch (InterruptedException e) {
                    // do nothing
                } finally {

                }

            }
        };
        splashTread.start();

    }

}
