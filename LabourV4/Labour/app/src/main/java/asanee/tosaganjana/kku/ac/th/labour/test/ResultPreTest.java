package asanee.tosaganjana.kku.ac.th.labour.test;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import asanee.tosaganjana.kku.ac.th.labour.main.BaseActivity;
import asanee.tosaganjana.kku.ac.th.labour.main.MainActivity;


public class ResultPreTest extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        updateDatabase();

    }

    private void updateDatabase(){
        Bundle bundle = getIntent().getExtras();
        int score = bundle.getInt("score");

        String UID = FirebaseAuth.getInstance().getCurrentUser().getUid();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(UID).child("information");

        myRef.child("pre test").setValue(String.valueOf(score));

        goBack(MainActivity.class);
    }
}
