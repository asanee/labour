package asanee.tosaganjana.kku.ac.th.labour.main.signup;

import android.app.Activity;
import android.content.DialogInterface;
import android.net.Uri;
import android.support.v4.app.DialogFragment;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import asanee.tosaganjana.kku.ac.th.labour.R;
import asanee.tosaganjana.kku.ac.th.labour.main.BaseActivity;
import asanee.tosaganjana.kku.ac.th.labour.main.MainActivity;
import asanee.tosaganjana.kku.ac.th.labour.main.date.DatePickerFragment;
import asanee.tosaganjana.kku.ac.th.labour.main.LoginActivity;



public class SignUpActivity extends BaseActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private Spinner spinnerProvince, spinnerDistinct, spinnerSubDistinct;
    private ArrayList<String> mProvince = new ArrayList<String>();
    private ArrayList<String> mDistinct = new ArrayList<String>();
    private ArrayList<String> mSubDistinct = new ArrayList<String>();
    private final static String SELECT = "-- pleases select --";

    private String name, lastName, tel, birthDate, id;
    private String gestational, pregnancy, sickness;
    private String congenital = "";

    private String changwats, amphoes, tambons;
    private String changwatsPid, amphoesPid;

    private Activity activity;
    private ReadFileJSON readFileJSON;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        spinnerUpdate();
        init();

    }

    private void init(){

        findViewById(R.id.rbt_congenital_disorder_n).setOnClickListener(this);
        findViewById(R.id.rbt_congenital_disorder_y).setOnClickListener(this);
        findViewById(R.id.rbt_sickness_n).setOnClickListener(this);
        findViewById(R.id.rbt_sickness_y).setOnClickListener(this);
        findViewById(R.id.btn_sign_up).setOnClickListener(this);
        findViewById(R.id.btn_edit_date).setOnClickListener(this);

        activity = this;

    }


    private void spinnerUpdate() {

        spinnerProvince = (Spinner) findViewById(R.id.thai_province);
        spinnerDistinct = (Spinner) findViewById(R.id.thai_distinct);
        spinnerSubDistinct = (Spinner) findViewById(R.id.thai_sub_distinct);

        spinnerProvince.setOnItemSelectedListener(this);
        spinnerDistinct.setOnItemSelectedListener(this);
        spinnerSubDistinct.setOnItemSelectedListener(this);

        readFileJSON = new ReadFileJSON(this);

        mProvince.clear();
        changwatsPid = readFileJSON.getProvince(SELECT, mProvince);

        readFileJSON.sort(mProvince);
        ArrayAdapter<String> adapterProvince = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, mProvince);
        spinnerProvince.setAdapter(adapterProvince);

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.thai_province:
                changwats = mProvince.get(position);
                mProvince.clear();
                changwatsPid = readFileJSON.getProvince(changwats, mProvince);
                readFileJSON.sort(mProvince);
                mDistinct.clear();
                amphoesPid = readFileJSON.getDistinct(null, mDistinct, changwatsPid);
                readFileJSON.sort(mDistinct);
                ArrayAdapter<String> adapterDistinct = new ArrayAdapter<String>(this,
                        android.R.layout.simple_dropdown_item_1line, mDistinct);
                spinnerDistinct.setAdapter(adapterDistinct);
                break;
            case R.id.thai_distinct:
                amphoes = mDistinct.get(position);
                mDistinct.clear();
                amphoesPid = readFileJSON.getDistinct(amphoes, mDistinct, changwatsPid);
                readFileJSON.sort(mDistinct);
                mSubDistinct.clear();
                readFileJSON.getSubDistinct(mSubDistinct, amphoesPid);

                readFileJSON.sort(mSubDistinct);
                ArrayAdapter<String> adapterThai = new ArrayAdapter<String>(this,
                        android.R.layout.simple_dropdown_item_1line, mSubDistinct);
                spinnerSubDistinct.setAdapter(adapterThai);
                break;
            case R.id.thai_sub_distinct:
                tambons = mSubDistinct.get(position);
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_sign_up:
                submit();
                break;
            case R.id.rbt_congenital_disorder_n:
                congenitalHide();
                break;
            case R.id.rbt_congenital_disorder_y:
                congenitalShow();
                break;
            case R.id.rbt_sickness_n:
                sicknessHide();
                break;
            case R.id.rbt_sickness_y:
                sicknessShow();
                break;
            case R.id.btn_edit_date:
                pickDate();
                break;

        }


    }

    private void pickDate(){
        DialogFragment newFragment = new DatePickerFragment(getApplicationContext(), activity);
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    private void congenitalHide() {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.lay_congenital_disorder);
        linearLayout.setVisibility(View.GONE);
    }

    private void congenitalShow() {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.lay_congenital_disorder);
        linearLayout.setVisibility(View.VISIBLE);
    }

    private void sicknessHide() {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.lay_sickness);
        linearLayout.setVisibility(View.GONE);
    }

    private void sicknessShow() {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.lay_sickness);
        linearLayout.setVisibility(View.VISIBLE);
    }


    private void submit() {
        EditText edtID = (EditText) findViewById(R.id.edt_id);
        EditText edtName = (EditText) findViewById(R.id.edt_name);
        EditText edtLastName = (EditText) findViewById(R.id.edt_lastname);
        EditText edtTel = (EditText) findViewById(R.id.edt_tel);
        EditText edtDate = (EditText) findViewById(R.id.edt_date);
        EditText edtGestational = (EditText) findViewById(R.id.edt_gestational_age);
        EditText edtPregnancy = (EditText) findViewById(R.id.edt_pregnancy);

        try {
            if (edtID.getText().toString().isEmpty() ||
                    edtName.getText().toString().isEmpty() ||
                    edtLastName.getText().toString().isEmpty() ||
                    edtTel.getText().toString().isEmpty() ||
                    edtDate.getText().toString().isEmpty() ||
                    edtGestational.getText().toString().isEmpty() ||
                    edtPregnancy.getText().toString().isEmpty()


                    ) {

                showToast("All fields are required.");
                congenital = "";

            } else {

                id = edtID.getText().toString();
                name = edtName.getText().toString();
                lastName = edtLastName.getText().toString();
                tel = edtTel.getText().toString();
                birthDate = edtDate.getText().toString();
                pregnancy = edtPregnancy.getText().toString();
                gestational = edtGestational.getText().toString();

                getCongenital();
                getSickness();

                done();


            }
        } catch (Exception e) {
            showToast("All fields are required.");
            congenital = "";
        }


    }

    private void showToast(String test) {
        Toast.makeText(getApplicationContext(), test,
                Toast.LENGTH_SHORT).show();
    }

    private void getCongenital() {
        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.congenital_disorder);
        int radioButtonID = radioGroup.getCheckedRadioButtonId();

        RadioButton radioButton = (RadioButton) radioGroup.findViewById(radioButtonID);
        if (radioButton.getText().toString().equals("มีโรคประจำตัว")) {
            CheckBox item1 = (CheckBox) findViewById(R.id.cb_item_1);
            CheckBox item2 = (CheckBox) findViewById(R.id.cb_item_2);
            CheckBox item3 = (CheckBox) findViewById(R.id.cb_item_3);
            CheckBox item4 = (CheckBox) findViewById(R.id.cb_item_4);

            insertCongenital(item1);
            insertCongenital(item2);
            insertCongenital(item3);
            insertCongenital(item4);

            EditText edtCongenital = (EditText) findViewById(R.id.edt_congenital_disorder);

            if (!edtCongenital.getText().toString().isEmpty()) {
                congenital += edtCongenital.getText().toString() + ", ";
            }
        } else {
            congenital = radioButton.getText().toString();

        }

    }

    private void insertCongenital(CheckBox checkBox) {
        if (checkBox.isChecked()) {
            congenital += checkBox.getText().toString() + ", ";
        }
    }

    private void getSickness() {
        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.sickness);
        int radioButtonID = radioGroup.getCheckedRadioButtonId();

        RadioButton radioButton = (RadioButton) radioGroup.findViewById(radioButtonID);
        if (radioButton.getText().toString().equals("มี")) {

            EditText edtSickness = (EditText) findViewById(R.id.edt_sickness);

            if (!edtSickness.getText().toString().isEmpty()) {
                sickness = edtSickness.getText().toString();
            }
        } else {
            sickness = radioButton.getText().toString();

        }

    }

    private void done() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setMessage("You want to submit.");
        alert.setCancelable(false);
        alert.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                saveEditDone();
            }
        });
        alert.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alert.show();
    }

    private void saveEditDone() {
        if (isValidate()) {
            showProgressDialog();

            String UID = FirebaseAuth.getInstance().getCurrentUser().getUid();
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            final DatabaseReference myRef = database.getReference(UID).child("information");

            myRef.child("name").setValue(name);
            myRef.child("lastname").setValue(lastName);
            myRef.child("birthdate").setValue(birthDate);
            myRef.child("tel").setValue(tel);
            myRef.child("address").child("province").setValue(changwats);
            myRef.child("address").child("distinct").setValue(amphoes);
            myRef.child("address").child("sub distinct").setValue(tambons);
            myRef.child("id").setValue(id);
            myRef.child("sickness").setValue(sickness);
            myRef.child("congenital").setValue(congenital.substring(0, congenital.length() - 2));
            myRef.child("pregnancy").setValue(pregnancy);
            myRef.child("gestational").setValue(gestational);

            StorageReference mStorageRef = FirebaseStorage.getInstance().getReference();
            StorageReference imageRef = mStorageRef.child("profile.JPEG");
            imageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    String url = uri.toString();
                    myRef.child("image").setValue(url);

                }
            });


            hideProgressDialog();

            goTo(MainActivity.class);

        }
    }

    private boolean isValidate() {
        if (id.length() != 13) {
            Toast.makeText(getApplicationContext(), "Your Id is Invalid .",
                    Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }


}
