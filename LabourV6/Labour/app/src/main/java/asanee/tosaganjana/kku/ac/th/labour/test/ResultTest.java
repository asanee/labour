package asanee.tosaganjana.kku.ac.th.labour.test;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import asanee.tosaganjana.kku.ac.th.labour.R;
import asanee.tosaganjana.kku.ac.th.labour.main.BaseActivity;
import asanee.tosaganjana.kku.ac.th.labour.main.MainActivity;
import de.hdodenhof.circleimageview.CircleImageView;

public class ResultTest extends BaseActivity implements View.OnClickListener {

    private TextView text1, text2;
    private CircleImageView color;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_test);

        init();
        getScore();


    }

    private void init() {
        findViewById(R.id.test_finish).setOnClickListener(this);
        text1 = (TextView) findViewById(R.id.text1);
        text2 = (TextView) findViewById(R.id.text2);
        color = (CircleImageView) findViewById(R.id.color);
    }

    private void getScore() {
        Bundle bundle = getIntent().getExtras();
        int score = bundle.getInt("score");

        TextView text1 = (TextView) findViewById(R.id.text1);
        TextView text2 = (TextView) findViewById(R.id.text2);
        CircleImageView color = (CircleImageView) findViewById(R.id.color);


        if (score < 2) {
            color.setImageResource(R.color.green); //green score less than 2
            text1.setText("ไม่มีอาการผิดปกติ ไปพบแพทย์ตามนัดครั้งถัดไป");
        } else if (score >= 2 && score < 10) {
            color.setImageResource(R.color.yellow); //yellow score more than 2 and less than 10

            text1.setText("เฝ้าระวังอาการต่อไป หรือ ถ้ามีข้อสงสัยโทรปรึกษาได้ที่เบอร์ 02-7634017");
        } else if (score >= 10) {
            color.setImageResource(R.color.red); //red score more than 10
            text1.setText("ไปโรงพยาบาล พร้อมกับเอกสาร");
            text2.setText(
                    "1. สมุดบันทึกสุขภาพแม่และเด็ก\n" +
                            "2. บัตรประจำตัวประชาชน\n" +
                            "3. สำเนาทะเบียนบ้าน สำหรับทำสูติบัตรลูก (แจ้งเกิด)");
        }
    }

    @Override
    public void onClick(View v) {
        goBack(MainActivity.class);
    }
}
