package asanee.tosaganjana.kku.ac.th.labour.main.signup;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import asanee.tosaganjana.kku.ac.th.labour.R;
import asanee.tosaganjana.kku.ac.th.labour.main.LoginActivity;

public class SignUpEmailActivity extends LoginActivity implements View.OnClickListener {
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_email);
        mAuth = FirebaseAuth.getInstance();

        findViewById(R.id.btn_sign_up).setOnClickListener(this);

        updateFirebase();

    }

    @Override
    public void onClick(View v) {

        submit();
    }

    private void updateFirebase(){
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d("UID", user.getUid());

                    goTo(SignUpActivity.class);

                }
            }
        };
    }

    private void submit(){

        EditText edtEmail = (EditText) findViewById(R.id.edt_email);
        EditText edtPassword = (EditText) findViewById(R.id.edt_password);
        EditText edtPasswordConfirm = (EditText) findViewById(R.id.edt_password_confirm);

        try {
            String email = edtEmail.getText().toString();
            String password = edtPassword.getText().toString();
            String passwordConfirm = edtPasswordConfirm.getText().toString();

            if (isValidate(email, password, passwordConfirm)) {

                showProgressDialog();
                createAccount(email, password);
                signIn(email, password);

            }
        } catch (Exception e) {

            Toast.makeText(getApplicationContext(), "All fields are required.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    private boolean isValidate(String email, String password, String passwordConfirm) {
        if (!isEmailValid(email)) {
            Toast.makeText(getApplicationContext(), "Your Email is Invalid.",
                    Toast.LENGTH_SHORT).show();
            return false;
        } else if (password.length() < 6) {
            Toast.makeText(getApplicationContext(), "Passwords must be at least 6 characters .",
                    Toast.LENGTH_SHORT).show();
            return false;
        } else if (!password.equals(passwordConfirm)) {
            Toast.makeText(getApplicationContext(), "Both password doesn't match.",
                    Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }


    private void createAccount(String email, String password) {

        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (!task.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Email already use.",
                            Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Sign Up Success",
                            Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void signIn(String email, String password) {

        showProgressDialog();
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if (!task.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Sign in fail",
                            Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Sign in Success",
                            Toast.LENGTH_SHORT).show();
                }
                hideProgressDialog();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

}
