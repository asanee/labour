package asanee.tosaganjana.kku.ac.th.labour.preparing;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import asanee.tosaganjana.kku.ac.th.labour.R;
import asanee.tosaganjana.kku.ac.th.labour.data.DataDetailActivity;
import asanee.tosaganjana.kku.ac.th.labour.list.index.IndexAdapter;
import asanee.tosaganjana.kku.ac.th.labour.list.index.RecyclerItemClickListener;
import asanee.tosaganjana.kku.ac.th.labour.list.index.index;
import asanee.tosaganjana.kku.ac.th.labour.main.BaseActivity;
import asanee.tosaganjana.kku.ac.th.labour.main.MainActivity;

public class PreparingActivity extends BaseActivity implements View.OnClickListener {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    List<index> dataset = new ArrayList<index>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preparing);

        init();
        recyclerView();
    }

    private void init(){
        findViewById(R.id.back).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back:
                goBack(MainActivity.class);
                break;
        }
    }

    private List<index> initIndex() {

        index index1 = new index(1,getString(R.string.text_preparing_1_title),getString(R.string.text_preparing_1));
        dataset.add(index1);
        index index2 = new index(2,getString(R.string.text_preparing_2_title),"");
        dataset.add(index2);
        index index3 = new index(3,getString(R.string.text_preparing_3_title), "");
        dataset.add(index3);
        index index4 = new index(4,getString(R.string.text_preparing_4_title), getString(R.string.text_pain_2));
        dataset.add(index4);
//        index index5 = new index(5,getString(R.string.text_preparing_5_title),"");
//        dataset.add(index5);

        return dataset;
    }

    @Override
    public void onBackPressed() {  goBack(MainActivity.class); }


    private void recyclerView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);


        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new IndexAdapter(this, initIndex());
        mRecyclerView.setAdapter(mAdapter);


        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this,
                mRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                if(dataset.get(position).getIndex() == 2) {
                    goTo(PreparingActivityData2.class);
                }
                else if(dataset.get(position).getIndex() == 3) {
                    goTo(PreparingActivityData3.class);
                }
                else if(dataset.get(position).getIndex() == 4) {
                    goTo(PreparingActivityData4.class);
                }
//                else if(dataset.get(position).getIndex() == 5) {
//                    goTo(PreparingActivityData5.class);
//                }
                else {
                    goToData(DataDetailActivity.class, dataset.get(position).getTitle(),
                            dataset.get(position).getDetail());
                }
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));
    }
}
