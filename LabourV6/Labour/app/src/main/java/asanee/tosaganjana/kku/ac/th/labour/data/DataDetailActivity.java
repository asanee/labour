package asanee.tosaganjana.kku.ac.th.labour.data;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import asanee.tosaganjana.kku.ac.th.labour.R;
import asanee.tosaganjana.kku.ac.th.labour.emergency.EmergencyActivity;
import asanee.tosaganjana.kku.ac.th.labour.main.BaseActivity;
import asanee.tosaganjana.kku.ac.th.labour.main.MainActivity;
import asanee.tosaganjana.kku.ac.th.labour.pain.PainActivity;

public class DataDetailActivity extends BaseActivity implements View.OnClickListener {
    String title;
    String data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_detail);

        init();
    }

    private void init(){
        title = getIntent().getExtras().getString("title");
        data = getIntent().getExtras().getString("data");

        TextView tvTitle = (TextView) findViewById(R.id.title);
        tvTitle.setText(title);

        TextView tvData = (TextView) findViewById(R.id.data);
        tvData.setText(Html.fromHtml(data));

        findViewById(R.id.back).setOnClickListener(this);
        findViewById(R.id.home).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back:
                super.onBackPressed();
                break;
            case R.id.home:
                goBack(MainActivity.class);
                break;
        }

    }

}
