package asanee.tosaganjana.kku.ac.th.labour.emergency;

import android.content.Intent;
import android.support.constraint.solver.Cache;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import asanee.tosaganjana.kku.ac.th.labour.R;
import asanee.tosaganjana.kku.ac.th.labour.data.DataDetailActivity;
import asanee.tosaganjana.kku.ac.th.labour.main.BaseActivity;
import asanee.tosaganjana.kku.ac.th.labour.main.MainActivity;

public class EmergencyActivity extends BaseActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency);

        init();
        updateView();
    }

    private void init(){
//       findViewById(R.id.btn_emergency_1).setOnClickListener(this);
//       findViewById(R.id.btn_emergency_2).setOnClickListener(this);
       findViewById(R.id.back).setOnClickListener(this);

    }

    private void updateView(){
        TextView tvData = (TextView) findViewById(R.id.data);
        tvData.setText(Html.fromHtml(getString(R.string.text_emergency)));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.btn_emergency_1:
//                goToData(DataDetailActivity.class,getString(R.string.labour_emergency),
//                        getString(R.string.text_emergency_1));
//                break;
//            case R.id.btn_emergency_2:
//                goToData(DataDetailActivity.class,getString(R.string.labour_emergency),
//                        getString(R.string.text_emergency_2));
//                break;
            case R.id.back:
                goBack(MainActivity.class);
                break;

        }
    }

    @Override
    public void onBackPressed() {  goBack(MainActivity.class); }


}
