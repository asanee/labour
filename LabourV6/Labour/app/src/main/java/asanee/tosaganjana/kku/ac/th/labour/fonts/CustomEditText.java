package asanee.tosaganjana.kku.ac.th.labour.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

/**
 * Created by Puppymind on 30/4/2560.
 */

public class CustomEditText extends android.support.v7.widget.AppCompatEditText {

    public CustomEditText(Context context) {
        super(context);
        initTypFace();
    }
    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        initTypFace();
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initTypFace();
    }

    private void initTypFace() {
        Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), "THSarabun.ttf");
        setTypeface(typeface);
    }
}
