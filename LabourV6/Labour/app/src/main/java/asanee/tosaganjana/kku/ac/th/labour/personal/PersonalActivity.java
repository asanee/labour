package asanee.tosaganjana.kku.ac.th.labour.personal;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.StringSignature;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import asanee.tosaganjana.kku.ac.th.labour.R;
import asanee.tosaganjana.kku.ac.th.labour.fonts.CustomButton;
import asanee.tosaganjana.kku.ac.th.labour.main.MainActivity;
import asanee.tosaganjana.kku.ac.th.labour.personal.admin.AdminActivity;
import asanee.tosaganjana.kku.ac.th.labour.personal.admin.PersonalTestActivity;


public class PersonalActivity extends MainActivity implements View.OnClickListener {

    String url, role;
    Activity activity;
    String UID;
    private ExpandableRelativeLayout mExpandLayoutPre, mExpandLayoutPost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle("ข้อมูลส่วนตัว");

        initDrawerLayout(null);
        init();
        upDateUI();

        activity = this;
        SimpleDateFormat myFormat = new SimpleDateFormat("dd MM yyyy");
        String inputString1 = "23 01 1997";
        String inputString2 = "27 04 1997";
        try {
            Date date1 = myFormat.parse(inputString1);
            Date date2 = myFormat.parse(inputString2);
            long diff = date2.getTime() - date1.getTime();
            Log.d("date","Days: " + (280 - TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)) / 7);
        }  catch (ParseException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.fab_edit:
                goTo(PersonalEditActivity.class);
                break;
            case R.id.back:
                back();
                break;
            case R.id.expandButton_pre:
                mExpandLayoutPre.toggle();
                break;
            case R.id.expandButton_post:
                mExpandLayoutPost.toggle();
                break;
            case R.id.fab_permission:
                permission();
                break;
            case R.id.pre_more:
                goToUID(PersonalTestActivity.class, "testType", "pre_test", UID);
                break;
            case R.id.post_more:
                goToUID(PersonalTestActivity.class, "testType", "post_test", UID);
                break;

        }
    }

    @Override
    public void onBackPressed() {
        back();
    }


    private void init() {

        findViewById(R.id.fab_edit).setOnClickListener(this);
        findViewById(R.id.fab_permission).setOnClickListener(this);
        findViewById(R.id.back).setOnClickListener(this);
        findViewById(R.id.expandButton_pre).setOnClickListener(this);
        findViewById(R.id.expandButton_post).setOnClickListener(this);
        findViewById(R.id.pre_more).setOnClickListener(this);
        findViewById(R.id.post_more).setOnClickListener(this);
        mExpandLayoutPre = (ExpandableRelativeLayout) findViewById(R.id.expandableLayout_pre);
        mExpandLayoutPost = (ExpandableRelativeLayout) findViewById(R.id.expandableLayout_post);
    }

    private void upDateUI() {

        showProgressDialog();

        final TextView tvId = (TextView) findViewById(R.id.personal_id);
        final TextView tvName = (TextView) findViewById(R.id.personal_name);
        final TextView tvLastName = (TextView) findViewById(R.id.personal_lastname);
        final TextView tvAge = (TextView) findViewById(R.id.personal_age);
        final TextView tvAddress = (TextView) findViewById(R.id.personal_address);
        final TextView tvTel = (TextView) findViewById(R.id.personal_tel);
        final TextView tvSickness = (TextView) findViewById(R.id.personal_sickness);
        final TextView tvCongenital = (TextView) findViewById(R.id.personal_congenital_disorder);
        final TextView tvGestational = (TextView) findViewById(R.id.personal_gestational_age);
        final TextView tvPregnancy = (TextView) findViewById(R.id.personal_pregnancy);
        final TextView tvPreTest1 = (TextView) findViewById(R.id.personal_pre_test_part_1);
        final TextView tvPreTest2 = (TextView) findViewById(R.id.personal_pre_test_part_2);
        final TextView tvPreTest3 = (TextView) findViewById(R.id.personal_pre_test_part_3);
        final TextView tvPostTest1 = (TextView) findViewById(R.id.personal_post_test_part_1);
        final TextView tvPostTest2 = (TextView) findViewById(R.id.personal_post_test_part_2);
        final TextView tvPostTest3 = (TextView) findViewById(R.id.personal_post_test_part_3);
        final TextView tvEmail = (TextView) findViewById(R.id.personal_email);
        final TextView tvLine = (TextView) findViewById(R.id.personal_line);
        final TextView tvBirthRecord = (TextView) findViewById(R.id.personal_birth_record);
        final TextView tvReligion = (TextView) findViewById(R.id.personal_religion);
        final TextView tvEducation = (TextView) findViewById(R.id.personal_education);
        final TextView tvJob = (TextView) findViewById(R.id.personal_job);
        final TextView tvSalary = (TextView) findViewById(R.id.personal_salary);
        final ImageView imageView = (ImageView) findViewById(R.id.bgheader);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_edit);
        FloatingActionButton fabPermission = (FloatingActionButton) findViewById(R.id.fab_permission);
        CustomButton preMore = (CustomButton) findViewById(R.id.pre_more);
        final CustomButton postMore = (CustomButton) findViewById(R.id.post_more);

        UID = FirebaseAuth.getInstance().getCurrentUser().getUid();

        /** check role */
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            UID = bundle.getString("key");
            fab.setVisibility(View.GONE);
            fabPermission.setVisibility(View.VISIBLE);
            preMore.setVisibility(View.VISIBLE);
            postMore.setVisibility(View.VISIBLE);
        } else {
            fab.setVisibility(View.VISIBLE);
            fabPermission.setVisibility(View.GONE);
            preMore.setVisibility(View.GONE);
            postMore.setVisibility(View.GONE);
        }

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(UID);


        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                DataSnapshot information = dataSnapshot.child("information");

                String id = information.child("id").getValue(String.class);
                String name = information.child("name").getValue(String.class);
                String lastName = information.child("lastname").getValue(String.class);
                String province = information.child("address").child("province").getValue(String.class);
                String distinct = information.child("address").child("distinct").getValue(String.class);
                String subDistinct = information.child("address").child("sub distinct").getValue(String.class);
                String birthDate = information.child("birthdate").getValue(String.class);
                String tel = information.child("tel").getValue(String.class);
                String email = information.child("email").getValue(String.class);
                String line = information.child("line").getValue(String.class);
                String pregnancy = information.child("pregnancy").getValue(String.class);
                String sickness = information.child("sickness").getValue(String.class);
                String gestational = information.child("gestational").getValue(String.class);
                String congenital = information.child("congenital").getValue(String.class);
                String birthRecord = information.child("birthRecord").getValue(String.class);
                String religion = information.child("religion").getValue(String.class);
                String education = information.child("education").getValue(String.class);
                String job = information.child("job").getValue(String.class);
                String salary = information.child("salary").getValue(String.class);
                String preTest1 = information.child("pre_test").child("part_1").child("score").getValue(String.class);
                String preTest2 = information.child("pre_test").child("part_2").child("score").getValue(String.class);
                String preTest3 = information.child("pre_test").child("part_3").child("score").getValue(String.class);
                String postTest1 = information.child("post_test").child("part_1").child("score").getValue(String.class);
                String postTest2 = information.child("post_test").child("part_2").child("score").getValue(String.class);
                String postTest3 = information.child("post_test").child("part_3").child("score").getValue(String.class);

                String[] birthRecords = null;
                try {
                    birthRecords = birthRecord.split(",");
                } catch (Exception e) {

                }
                role = dataSnapshot.child("role").getValue(String.class);
                url = information.child("image").getValue(String.class);

                if (url != null) {
                    Glide.with(PersonalActivity.this)
                            .load(url)
                            .centerCrop()
                            .signature(new StringSignature(UUID.randomUUID().toString()))
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .into(imageView);
                }

                if (postTest1 == null || postTest2 == null || postTest3 == null) {
                    postMore.setVisibility(View.GONE);
                }


                tvPreTest1.setText(getScore(preTest1) + "/50");
                tvPreTest2.setText(getScore(preTest2) + "/11");
                tvPreTest3.setText(getScore(preTest3) + "/10");
                tvPostTest1.setText(getScore(postTest1) + "/50");
                tvPostTest2.setText(getScore(postTest2) + "/11");
                tvPostTest3.setText(getScore(postTest3) + "/10");

                tvId.setText(": " + id);
                tvName.setText(": " + name);
                tvLastName.setText(": " + lastName);
                tvAddress.setText(": " + "ต." + subDistinct + " อ." + distinct + " จ." + province);
                tvTel.setText(": " + tel);
                tvAge.setText(": " + getAge(birthDate) + " \t\tปี");
                tvSickness.setText(": " + sickness);
                tvCongenital.setText(": " + congenital);
                tvPregnancy.setText(": " + pregnancy);
                tvGestational.setText(": " + getGestational(gestational));
                tvEmail.setText(": " + email);
                tvLine.setText(": " + line);
                tvBirthRecord.setText(getBirthRecord(birthRecords));
                tvReligion.setText(": " + religion);
                tvEducation.setText(": " + education);
                tvJob.setText(": " + job);
                tvSalary.setText(": " + salary + "\t\tบาท/เดือน");

                hideProgressDialog();
            }

            @Override
            public void onCancelled(DatabaseError error) {


            }
        });

    }

    public String getBirthRecord(String[] records) {
        String result = "";
        try {
            for (int i = 0; i < records.length; i++) {
                result += "ครรภ์ที่ " + (i + 1) + " \t\t" + records[i] + "\n";
            }
        } catch (Exception e) {

        }
        return result;
    }

    private void permission() {

//        android.app.AlertDialog.Builder builder =
//                new android.app.AlertDialog.Builder(activity);
        AlertDialog.Builder builder =
                new AlertDialog.Builder(activity);

        LayoutInflater inflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View viewDialog = inflater.inflate(R.layout.dialog_role, null);

        builder.setView(viewDialog);

        RadioButton rdtAdmin = (RadioButton) viewDialog.findViewById(R.id.role_admin);
        RadioButton rdtUsesr = (RadioButton) viewDialog.findViewById(R.id.role_user);

        try {
            switch (role) {
                case "Admin":
                    rdtAdmin.setChecked(true);
                    break;
                case "User":
                    rdtUsesr.setChecked(true);
                    break;
            }
        } catch (Exception e) {

        }

        final RadioGroup radioGroup = (RadioGroup) viewDialog.findViewById(R.id.role);


        builder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                int radioButtonID = radioGroup.getCheckedRadioButtonId();

                RadioButton radioButton = (RadioButton) radioGroup.findViewById(radioButtonID);
                String role = radioButton.getText().toString();

                /** Add permission */
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                database.getReference(UID).child("role").setValue(role);

//                Log.d("role",radioButton.getText().toString());

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.create();

        builder.show();
    }

    public String getScore(String score) {
        String str;
        if (score == null) {
            str = " - ";
            return str;
        } else {
            str =  score;
            return str;
        }


    }
    private String getGestational(String due){
        SimpleDateFormat myFormat = new SimpleDateFormat("dd-MM-yyyy");
        long weekGestationalAge = 0;
        long dayGestationalAge = 0;
//        due = "29-8-2017";
        try {
            Date dueDate = myFormat.parse(due);
            Date date = new Date();
            long diff = dueDate.getTime() - date.getTime();
            long diffDay = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) + 1;
            weekGestationalAge = (280 - diffDay) / 7;
            dayGestationalAge = (280 - diffDay) % 7;
        }  catch (ParseException e) {
            e.printStackTrace();
        }
        if(dayGestationalAge == 0){
            return weekGestationalAge + " \t\t สัปดาห์";
        }
        return weekGestationalAge + " \t\t สัปดาห์ \t\t" + dayGestationalAge + "\t\t วัน";
    }

    public String getAge(String birthdate) {
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        Date date = null;
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");

        try {
            date = format.parse(birthdate);

        } catch (Exception e) {
            e.printStackTrace();
        }

        dob.setTime(date);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }

        Integer ageInt = new Integer(age);
        String ageS = ageInt.toString();

        return ageS;
    }

    public void back() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            goBack(AdminActivity.class);
        } else {
            goBack(MainActivity.class);
        }
    }


    public void goBack(Class mClass) {

        Intent intent = new Intent(this, mClass);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_righ);
    }


}
