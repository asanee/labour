package asanee.tosaganjana.kku.ac.th.labour.personal.admin;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.Scanner;

import asanee.tosaganjana.kku.ac.th.labour.R;
import asanee.tosaganjana.kku.ac.th.labour.main.BaseActivity;
import asanee.tosaganjana.kku.ac.th.labour.main.MainActivity;

public class PersonalTestActivity extends BaseActivity implements View.OnClickListener {

    private StringBuilder builder;
    private JSONObject exam;
    private JSONArray partArray;
    private ExpandableRelativeLayout mExpandLayoutPart1, mExpandLayoutPart2, mExpandLayoutPart3;
    private String testType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_test);

        init();
        updateDatabase();

    }

    @Override
    public void onClick(final View v) {
        switch (v.getId()) {
            case R.id.expandButton_part_1:
                mExpandLayoutPart1.toggle();
                break;
            case R.id.expandButton_part_2:
                mExpandLayoutPart2.toggle();
                break;
            case R.id.expandButton_part_3:
                mExpandLayoutPart3.toggle();
                break;
            case R.id.back:
                super.onBackPressed();
                break;
            case R.id.home:
                goBack(MainActivity.class);
                break;
        }
    }


    private void init() {
        findViewById(R.id.expandButton_part_1).setOnClickListener(this);
        findViewById(R.id.expandButton_part_2).setOnClickListener(this);
        findViewById(R.id.expandButton_part_3).setOnClickListener(this);
        findViewById(R.id.back).setOnClickListener(this);
        findViewById(R.id.home).setOnClickListener(this);

        mExpandLayoutPart1 = (ExpandableRelativeLayout) findViewById(R.id.expandableLayout_part_1);
        mExpandLayoutPart2 = (ExpandableRelativeLayout) findViewById(R.id.expandableLayout_part_2);
        mExpandLayoutPart3 = (ExpandableRelativeLayout) findViewById(R.id.expandableLayout_part_3);

        testType = getIntent().getExtras().getString("testType");

        TextView title = (TextView) findViewById(R.id.title);
        title.setText(getTestTitle(testType));

    }

    private String getTestTitle(String testType){
        String str = "";
        switch (testType){
            case "pre_test":
                str = "PRE TEST";
                break;
            case "post_test":
                str = "POST TEST";
                break;
        }
        return str;
    }


    private void updateDatabase() {
        showProgressDialog();

        Bundle bundle = getIntent().getExtras();
        String UID = bundle.getString("UID");
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(UID).child("information");


        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                String testPart1 = dataSnapshot.child(testType).child("part_1").child("do").getValue(String.class);
                String scorePart1 = dataSnapshot.child(testType).child("part_1").child("score").getValue(String.class);
                String testPart2 = dataSnapshot.child(testType).child("part_2").child("do").getValue(String.class);
                String scorePart2 = dataSnapshot.child(testType).child("part_2").child("score").getValue(String.class);
                String testPart3 = dataSnapshot.child(testType).child("part_3").child("do").getValue(String.class);
                String scorePart3 = dataSnapshot.child(testType).child("part_3").child("score").getValue(String.class);

                String[] ans1 = testPart1.split(",");
                String[] ans2 = testPart2.split(",");
                String[] ans3 = testPart3.split(",");
//                Log.d("do", String.valueOf(parts.length));
                hideProgressDialog();

                String part1 = "ทัศนคติเกี่ยวกับการตั้งครรภ์";
                String part2 = "ความรู้ทั่วไปของหญิงตั้งครรภ์ไตรมาสที่ 3";
                String part3 = "พฤติกรรมสุขภาพของหญิงตั้งครรภ์ในไตรมาสที่ 3";

                TextView tvPart1 = (TextView) findViewById(R.id.part1);
                TextView tvPart2 = (TextView) findViewById(R.id.part2);
                TextView tvPart3 = (TextView) findViewById(R.id.part3);

                TextView tvScorePart1 = (TextView) findViewById(R.id.score_part1);
                TextView tvScorePart2 = (TextView) findViewById(R.id.score_part2);
                TextView tvScorePart3 = (TextView) findViewById(R.id.score_part3);

                builder();
                tvPart1.setText(readFile(builder.toString(), part1, ans1));
                tvPart2.setText(readFile(builder.toString(), part2, ans2));
                tvPart3.setText(readFile(builder.toString(), part3, ans3));

                tvScorePart1.setText("คะแนน : " + scorePart1);
                tvScorePart2.setText("คะแนน : " + scorePart2);
                tvScorePart3.setText("คะแนน : " + scorePart3);

            }

            @Override
            public void onCancelled(DatabaseError error) {


            }
        });

    }

    private void builder() {
        Resources res = getResources();
        InputStream is = res.openRawResource(R.raw.pretest);
        Scanner scanner = new Scanner(is);

        builder = new StringBuilder();

        while (scanner.hasNextLine()) {
            builder.append(scanner.nextLine());
        }

    }

    public String readFile(String s, String part, String[] ans) {
        String result = "";
        try {
            JSONObject root = new JSONObject(s);
            exam = root.getJSONObject("preTest2");
            partArray = (JSONArray) exam.get(part);

            for (int i = 0; i < ans.length; i++) {

                String question = partArray.getJSONObject(i).getString("problem" + String.valueOf(i + 1));
                String answer = ans[i];

                result += question + "\n\n\t\t\t - " + answer + "\n\n";

            }


        } catch (JSONException e) {

        }

        return result;
    }
}
