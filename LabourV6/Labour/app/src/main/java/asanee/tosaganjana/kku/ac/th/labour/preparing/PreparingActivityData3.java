package asanee.tosaganjana.kku.ac.th.labour.preparing;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import asanee.tosaganjana.kku.ac.th.labour.R;
import asanee.tosaganjana.kku.ac.th.labour.main.BaseActivity;
import asanee.tosaganjana.kku.ac.th.labour.main.MainActivity;

public class PreparingActivityData3 extends BaseActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preparing_data3);

        init();
        updateView();
    }

    private void init(){

        findViewById(R.id.back).setOnClickListener(this);
        findViewById(R.id.home).setOnClickListener(this);
    }

    private void updateView() {
        TextView tvData1 = (TextView) findViewById(R.id.data1);
        TextView tvData2 = (TextView) findViewById(R.id.data2);
        TextView tvData3 = (TextView) findViewById(R.id.data3);
        TextView tvData4 = (TextView) findViewById(R.id.data4);
        TextView tvData5 = (TextView) findViewById(R.id.data5);
        TextView tvData6 = (TextView) findViewById(R.id.data6);
        TextView tvData7 = (TextView) findViewById(R.id.data7);
        TextView tvData8 = (TextView) findViewById(R.id.data8);
        TextView tvData9 = (TextView) findViewById(R.id.data9);

        tvData1.setText(Html.fromHtml(getString(R.string.text_preparing_3_data_1)));
        tvData2.setText(Html.fromHtml(getString(R.string.text_preparing_3_data_2)));
        tvData3.setText(Html.fromHtml(getString(R.string.text_preparing_3_data_3)));
        tvData4.setText(Html.fromHtml(getString(R.string.text_preparing_3_data_4)));
        tvData5.setText(Html.fromHtml(getString(R.string.text_preparing_3_data_5)));
        tvData6.setText(Html.fromHtml(getString(R.string.text_preparing_3_data_6)));
        tvData7.setText(Html.fromHtml(getString(R.string.text_preparing_3_data_7)));
        tvData8.setText(Html.fromHtml(getString(R.string.text_preparing_3_data_8)));
        tvData9.setText(Html.fromHtml(getString(R.string.text_preparing_3_data_9)));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back:
                super.onBackPressed();
                break;
            case R.id.home:
                goBack(MainActivity.class);
                break;
        }

    }
}
