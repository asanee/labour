package asanee.tosaganjana.kku.ac.th.labour.personal.admin;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import asanee.tosaganjana.kku.ac.th.labour.R;
import asanee.tosaganjana.kku.ac.th.labour.list.index.IndexAdminAdapter;
import asanee.tosaganjana.kku.ac.th.labour.list.index.RecyclerItemClickListener;
import asanee.tosaganjana.kku.ac.th.labour.list.index.indexAdmin;
import asanee.tosaganjana.kku.ac.th.labour.main.BaseActivity;
import asanee.tosaganjana.kku.ac.th.labour.main.MainActivity;
import asanee.tosaganjana.kku.ac.th.labour.personal.PersonalActivity;

public class AdminActivity extends BaseActivity implements View.OnClickListener {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    List<indexAdmin> dataset = new ArrayList<indexAdmin>();
    List<indexAdmin> dataResult = new ArrayList<indexAdmin>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        setTitle("ADMIN");

        upDateUI();
        recyclerView();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }


    @Override
    public void onClick(View v) {
        goBack(MainActivity.class);
    }

    @Override
    public void onBackPressed() {
        goBack(MainActivity.class);
    }


    private List<indexAdmin> initIndex(List<indexAdmin> dataset) {

        dataResult = dataset;

        return dataResult;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_sreach, menu);
        MenuItem item = menu.findItem(R.id.menuSearch);
        SearchView searchView = (SearchView) item.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                filter(newText);
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                goBack(MainActivity.class);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void upDateUI() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference();

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()) {
//                    Log.d("child", child.getKey());
                    String key = child.getKey();
                    String master = dataSnapshot.child(key).child("master").getValue(String.class);

                    if (master == null) {
                        DataSnapshot information = dataSnapshot.child(key).child("information");

                        String url = information.child("image").getValue(String.class);
                        String name = information.child("name").getValue(String.class);
                        String lastName = information.child("lastname").getValue(String.class);
                        String id = information.child("id").getValue(String.class);

                        if (url == null || name == null || lastName == null || id == null) {

                        } else {
                            indexAdmin index = new indexAdmin(name + " " + lastName, id, url, key);
                            dataset.add(index);
//                            item.add(key);
                        }
                    } else {
                        Log.d("master", master);
                    }
                }

                mLayoutManager = new LinearLayoutManager(AdminActivity.this);
                mRecyclerView.setLayoutManager(mLayoutManager);

                mAdapter = new IndexAdminAdapter(AdminActivity.this, initIndex(dataset));
                mRecyclerView.setAdapter(mAdapter);


            }

            @Override
            public void onCancelled(DatabaseError error) {
            }
        });
    }

    private void recyclerView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mRecyclerView.setHasFixedSize(true);


        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this,
                mRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                Intent intent = new Intent(getApplicationContext(), PersonalActivity.class);
                intent.putExtra("key", dataResult.get(position).getKey());
                startActivity(intent);
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));
    }

    public void filter(String text) {
        List<indexAdmin> filter = new ArrayList<indexAdmin>();

        filter.clear();
        if (text.isEmpty()) {
            filter.addAll(dataset);

        } else {
            text = text.toLowerCase();
            for (int i = 0; i < dataset.size(); i++) {

                if (dataset.get(i).getName().toLowerCase().contains(text) ||
                        dataset.get(i).getId().toLowerCase().contains(text)) {

                    filter.add(dataset.get(i));
                }
            }
        }

        mLayoutManager = new LinearLayoutManager(AdminActivity.this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new IndexAdminAdapter(AdminActivity.this, initIndex(filter));
        mRecyclerView.setAdapter(mAdapter);

    }

}
