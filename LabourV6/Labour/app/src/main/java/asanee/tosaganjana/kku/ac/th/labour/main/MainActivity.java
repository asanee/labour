package asanee.tosaganjana.kku.ac.th.labour.main;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.comix.overwatch.HiveProgressView;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rodolfonavalon.shaperipplelibrary.ShapeRipple;
import com.rodolfonavalon.shaperipplelibrary.model.Circle;

import java.util.ArrayList;

import asanee.tosaganjana.kku.ac.th.labour.R;
import asanee.tosaganjana.kku.ac.th.labour.emergency.EmergencyActivity;
import asanee.tosaganjana.kku.ac.th.labour.fonts.CustomTypefaceSpan;
import asanee.tosaganjana.kku.ac.th.labour.main.signup.SignUpActivity;
import asanee.tosaganjana.kku.ac.th.labour.pain.PainActivity;
import asanee.tosaganjana.kku.ac.th.labour.personal.PersonalActivity;
import asanee.tosaganjana.kku.ac.th.labour.personal.admin.AdminActivity;
import asanee.tosaganjana.kku.ac.th.labour.preparing.PreparingActivity;
import asanee.tosaganjana.kku.ac.th.labour.quickening.QuickeningActivity;
import asanee.tosaganjana.kku.ac.th.labour.test.PreTestActivity;
import asanee.tosaganjana.kku.ac.th.labour.test.ResultPreTest;
import asanee.tosaganjana.kku.ac.th.labour.test.TestActivity;
import de.hdodenhof.circleimageview.CircleImageView;


public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    public GoogleApiClient mGoogleApiClient;
    public static ArrayList<String> item = new ArrayList<String>();
    boolean initRipple = false;
    private HiveProgressView progressBar;
    private final String PRE_TEST = "pre_test";
    private final String POST_TEST = "post_test";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("");


        shapeRipple();
        initDrawerLayout(toolbar);
        init();

        configureGoogleSignIn();
    }

    private void init() {

        findViewById(R.id.labour_test).setOnClickListener(this);
        findViewById(R.id.labour_pain).setOnClickListener(this);
        findViewById(R.id.labour_emergency).setOnClickListener(this);
        findViewById(R.id.labour_quickening).setOnClickListener(this);
        findViewById(R.id.labour_preparing).setOnClickListener(this);
    }

    public void initDrawerLayout(Toolbar toolbar) {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerView = navigationView.getHeaderView(0);

        upDateNavigation(headerView);
    }


    public void upDateNavigation(View view) {

        showProgressDialog();

        final TextView tvName = (TextView) view.findViewById(R.id.nav_name);
        final TextView tvID = (TextView) view.findViewById(R.id.nav_id);
        final CircleImageView imageView = (CircleImageView) view.findViewById(R.id.nav_profile_image);
        progressBar = (HiveProgressView) view.findViewById(R.id.progress);
        String UID = FirebaseAuth.getInstance().getCurrentUser().getUid();
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(UID);

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                DataSnapshot information = dataSnapshot.child("information");
                String name = information.child("name").getValue(String.class);
                String lastName = information.child("lastname").getValue(String.class);
                String id = information.child("id").getValue(String.class);
                String url = information.child("image").getValue(String.class);
                String preTest = information.child("pre_test").child("part_1").child("score").getValue(String.class);
                String postTest = information.child("post_test").child("part_1").child("score").getValue(String.class);

                /** Role */
                String role = dataSnapshot.child("role").getValue(String.class);

                if (id == null) {
                    goTo(SignUpActivity.class);
                    finish();
                }

                else if(preTest == null) {
                    goTo(PreTestActivity.class, "test_type", PRE_TEST);
                    finish();
                }

                String email = FirebaseAuth.getInstance().getCurrentUser().getEmail();

                tvName.setText(name + " " + lastName);
                tvID.setText(email);


                if (url != null) {
                    showProgressDialog();
                    new ImageLoadTask(url, imageView, progressBar).execute();
                    hideProgressDialog();
                }


                onNavigationView(role, postTest);

                hideProgressDialog();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        Intent setIntent = new Intent(Intent.ACTION_MAIN);
        setIntent.addCategory(Intent.CATEGORY_HOME);
        setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(setIntent);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.labour_test:
                goTo(TestActivity.class);
                break;

            case R.id.labour_emergency:
                goTo(EmergencyActivity.class);
                break;

            case R.id.labour_pain:
                goTo(PainActivity.class);
                break;

            case R.id.labour_quickening:
                goTo(QuickeningActivity.class);
                break;

            case R.id.labour_preparing:
                goTo(PreparingActivity.class);
                break;
        }

    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.nav_labour_pain:
                goTo(PainActivity.class);
                break;

            case R.id.nav_labour_emergency:
                goTo(EmergencyActivity.class);
                break;

            case R.id.nav_labour_quickening:
                goTo(QuickeningActivity.class);
                break;

            case R.id.nav_signOut:
                signOut();
                break;

            case R.id.nav_personal:
                goTo(PersonalActivity.class);
                break;

            case R.id.nav_personal_admin:
                goTo(AdminActivity.class);
                break;

            case R.id.nav_post_test:
                goTo(PreTestActivity.class, "test_type", POST_TEST);
                break;

            case R.id.nav_labour_preparing:
                goTo(PreparingActivity.class);
                break;

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void signOut() {

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setMessage(R.string.logout);
        alert.setCancelable(false);
        alert.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // Firebase sign out
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
                // Google sign out
                Auth.GoogleSignInApi.signOut(mGoogleApiClient);
            }
        });
        alert.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alert.show();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        String TAG = "MainActivity";
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
        Toast.makeText(this, "Google Play Services error.", Toast.LENGTH_SHORT).show();
    }


    public void shapeRipple() {

        final ShapeRipple ripple = (ShapeRipple) findViewById(R.id.ripple);
        ripple.setRippleShape(new Circle());
        ripple.setRippleColor(Color.WHITE);
        ripple.setRippleFromColor(Color.WHITE);
        ripple.setEnableColorTransition(true);
        ripple.setEnableStrokeStyle(true);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                ripple.setRippleDuration(1000);
                initRipple = true;
                if (initRipple) {
                    ripple.setRippleDuration(7000);
                }
            }
        }, 1500);
    }

    public void goTo(Class mClass) {

        Intent intent = new Intent(this, mClass);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public void applyFontToMenuItem(MenuItem mi) {

        Typeface font = Typeface.createFromAsset(getAssets(), "THSarabun.ttf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        String title = (String) mi.getTitle();
        mNewTitle.setSpan(new RelativeSizeSpan(1.5f), 0, title.length(), 0); // set size
        mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle.toString());
    }

    public void onNavigationView(String role, String postTest) {

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu nav_Menu = navigationView.getMenu();

        for (int i = 0; i < nav_Menu.size(); i++) {
            MenuItem mi = nav_Menu.getItem(i);
            SubMenu subMenu = mi.getSubMenu();

            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);

                }
            }

            applyFontToMenuItem(mi);
        }

        try {
            if (role.equals("Admin")) {
                nav_Menu.findItem(R.id.nav_personal_admin).setVisible(true);

            } else {
                nav_Menu.findItem(R.id.nav_personal_admin).setVisible(false);
            }
        }catch (Exception e){

        }
        if (postTest == null) {
            nav_Menu.findItem(R.id.nav_post_test).setVisible(true);
        } else {
            nav_Menu.findItem(R.id.nav_post_test).setVisible(false);
        }
        navigationView.setNavigationItemSelectedListener(this);
    }

    public void configureGoogleSignIn() {
        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }


}
