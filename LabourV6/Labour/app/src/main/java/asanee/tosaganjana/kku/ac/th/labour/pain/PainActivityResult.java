package asanee.tosaganjana.kku.ac.th.labour.pain;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import asanee.tosaganjana.kku.ac.th.labour.R;

public class PainActivityResult extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pain_result);
    }
}
