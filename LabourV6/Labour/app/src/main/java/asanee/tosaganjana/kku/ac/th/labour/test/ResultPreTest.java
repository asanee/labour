package asanee.tosaganjana.kku.ac.th.labour.test;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import asanee.tosaganjana.kku.ac.th.labour.R;
import asanee.tosaganjana.kku.ac.th.labour.fonts.CustomButton;
import asanee.tosaganjana.kku.ac.th.labour.fonts.CustomTextView;
import asanee.tosaganjana.kku.ac.th.labour.main.BaseActivity;
import asanee.tosaganjana.kku.ac.th.labour.main.MainActivity;


public class ResultPreTest extends BaseActivity {

    private int scorePart1,scorePart2,scorePart3;
    private String ansPart1,ansPart2,ansPart3,testType;
    CustomTextView txtScore1,txtScore2,txtScore3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_pre_test);

        txtScore1 = (CustomTextView) findViewById(R.id.txtScore1);
        txtScore2 = (CustomTextView) findViewById(R.id.txtScore2);
        txtScore3 = (CustomTextView) findViewById(R.id.txtScore3);

        updateDatabase();
        CustomButton btnOK = (CustomButton) findViewById(R.id.btnOK);

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                goBack(MainActivity.class);
                finish();
            }
        });

        updateDatabase();

    }

    private void updateDatabase() {

        Bundle bundle = getIntent().getExtras();
        scorePart1 = bundle.getInt("scorePart1");
        scorePart2 = bundle.getInt("scorePart2");
        scorePart3 = bundle.getInt("scorePart3");
        ansPart1 = bundle.getString("ansPart1");
        ansPart2 = bundle.getString("ansPart2");
        ansPart3 = bundle.getString("ansPart3");
        testType = bundle.getString("test_type");

        txtScore1.setText(Html.fromHtml("<b>ส่วนที่ 1</b>" + "<br/> ทัศนคติเกี่ยวกับการตั้งครรภ์ :     <br/> ได้  " + scorePart1 + "/50" + "  คะแนน"));
        txtScore2.setText(Html.fromHtml("<b>ส่วนที่ 2</b>" + "<br/> ความรู้ทั่วไปของหญิงตั้งครรภ์ ไตรมาสที่ 3 :     <br/> ได้  " + scorePart2 + "/11" + "  คะแนน"));
        txtScore3.setText(Html.fromHtml("<b>ส่วนที่ 3</b>" + "<br/> พฤติกรรมสุขภาพของหญิงตั้งครรภ์ ในไตรมาสที่ 3 :   <br/>  ได้  " + scorePart3 + "/10" + "  คะแนน"));


        addDataToDatabase(testType);


    }

    private void addDataToDatabase(String testType){
        String UID = FirebaseAuth.getInstance().getCurrentUser().getUid();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(UID).child("information");

        myRef.child(testType).child("part_1").child("score").setValue(String.valueOf(scorePart1));
        myRef.child(testType).child("part_1").child("do").setValue(ansPart1);
        myRef.child(testType).child("part_2").child("score").setValue(String.valueOf(scorePart2));
        myRef.child(testType).child("part_2").child("do").setValue(ansPart2);
        myRef.child(testType).child("part_3").child("score").setValue(String.valueOf(scorePart3));
        myRef.child(testType).child("part_3").child("do").setValue(ansPart3);



    }
}
