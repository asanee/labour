package asanee.tosaganjana.kku.ac.th.labour.list.index;

/**
 * Created by Puppymind on 29/4/2560.
 */

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import asanee.tosaganjana.kku.ac.th.labour.R;
import asanee.tosaganjana.kku.ac.th.labour.fonts.CustomTextView;
import de.hdodenhof.circleimageview.CircleImageView;

import static asanee.tosaganjana.kku.ac.th.labour.main.signup.SignUpActivity.birthRecords;

public class IndexBirthRecordAdapter extends RecyclerView.Adapter<IndexBirthRecordAdapter.ViewHolder> {

    private List<indexBirthRecord> mIndices;
    private Context mContext;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mIndex;
        public RadioGroup radioGroup;
//        public CustomTextView mId;
//        public CircleImageView imageView;

        public ViewHolder(View view) {
            super(view);

            mIndex = (TextView) view.findViewById(R.id.index);
            radioGroup = (RadioGroup) view.findViewById(R.id.birth_record);
//            mId = (CustomTextView) view.findViewById(R.id.admin_id);
//            imageView = (CircleImageView) view.findViewById(R.id.image_circle);
        }
    }

    public IndexBirthRecordAdapter(Context context, List<indexBirthRecord> dataset) {
        mIndices = dataset;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.recycler_view_birth_record, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int position) {
        final indexBirthRecord index = mIndices.get(position);

        viewHolder.mIndex.setText(String.valueOf((index.getIndex() + 1) + "."));
        viewHolder.radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                Log.d("handle", String.valueOf(checkedId));
                RadioButton radioButton = (RadioButton) viewHolder.radioGroup.findViewById(checkedId);
                birthRecords[index.getIndex()] = radioButton.getText().toString().replaceAll("\n","");

//                Log.d("handle", birthRecords[index.getIndex()]);
//                try {
//                    Log.d("handle", birthRecords[0]);
//                    Log.d("handle", birthRecords[1]);
//                    Log.d("handle", birthRecords[2]);
//                    Log.d("handle", birthRecords[3]);
//                }catch (Exception e){
//
//                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mIndices.size();
    }




}