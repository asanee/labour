package asanee.tosaganjana.kku.ac.th.labour.main.date;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;

import java.util.Calendar;

import asanee.tosaganjana.kku.ac.th.labour.R;

/**
 * Created by Puppymind on 19/4/2560.
 */

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    Context mContext;
    Activity activity;
    int viewId;

    public DatePickerFragment(Context context, Activity activity,int viewId) {
        mContext = context;
        this.activity = activity;
        this.viewId = viewId;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
//        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);

//
    }

    public void onDateSet(DatePicker viewPicker, int year, int month, int day) {
        // Do something with the date chosen by the user
        Log.d("onDateSet", "Date " + year + " " + month + " " + day);

        String setDate = day + "-" + (month + 1) + "-" + year;
        EditText edtDate = (EditText) activity.findViewById(viewId);
        edtDate.setText(setDate);


    }


}