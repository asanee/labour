package asanee.tosaganjana.kku.ac.th.labour.main.signup;

import android.app.Activity;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import asanee.tosaganjana.kku.ac.th.labour.R;
import asanee.tosaganjana.kku.ac.th.labour.list.index.IndexBirthRecordAdapter;
import asanee.tosaganjana.kku.ac.th.labour.list.index.RecyclerItemClickListener;
import asanee.tosaganjana.kku.ac.th.labour.list.index.indexBirthRecord;
import asanee.tosaganjana.kku.ac.th.labour.main.BaseActivity;
import asanee.tosaganjana.kku.ac.th.labour.main.MainActivity;
import asanee.tosaganjana.kku.ac.th.labour.main.date.DatePickerFragment;


public class SignUpActivity extends BaseActivity implements View.OnClickListener,
        AdapterView.OnItemSelectedListener, RadioGroup.OnCheckedChangeListener {

    private Spinner spinnerProvince, spinnerDistinct, spinnerSubDistinct, spinnerEducation, spinnerJob, spinnerPregnancy;
    private ArrayList<String> mProvince = new ArrayList<String>();
    private ArrayList<String> mDistinct = new ArrayList<String>();
    private ArrayList<String> mSubDistinct = new ArrayList<String>();
    private ArrayList<String> mEducation = new ArrayList<String>();
    private ArrayList<String> mJob = new ArrayList<String>();
    private ArrayList<Integer> mPregnancy = new ArrayList<Integer>();
    private final static String SELECT = "-- pleases select --";

    private String name, lastName, tel, birthDate, email, line, salary, religion;
    private String gestational, pregnancy, sickness, birthRecord = "";
    private String congenital = "";
    private String key;

    private String changwats, amphoes, tambons, education, job;
    private String changwatsPid, amphoesPid;

    private Activity activity;
    private ReadFileJSON readFileJSON;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    public static String[] birthRecords;
    List<indexBirthRecord> dataset = new ArrayList<indexBirthRecord>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        spinnerUpdate();
        init();


        getKey();

    }

    private void init() {

        findViewById(R.id.rbt_congenital_disorder_n).setOnClickListener(this);
        findViewById(R.id.rbt_congenital_disorder_y).setOnClickListener(this);
        findViewById(R.id.rbt_sickness_n).setOnClickListener(this);
        findViewById(R.id.rbt_sickness_y).setOnClickListener(this);
        findViewById(R.id.btn_sign_up).setOnClickListener(this);
        findViewById(R.id.btn_edit_date).setOnClickListener(this);
        findViewById(R.id.btn_edit_gestational_age).setOnClickListener(this);
        findViewById(R.id.rbt_religion_other).setOnClickListener(this);
        findViewById(R.id.cb_edit_congenital_disorder).setOnClickListener(this);

        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.religion);
        radioGroup.setOnCheckedChangeListener(this);

        activity = this;

    }



    private void spinnerUpdate() {

        spinnerProvince = (Spinner) findViewById(R.id.thai_province);
        spinnerDistinct = (Spinner) findViewById(R.id.thai_distinct);
        spinnerSubDistinct = (Spinner) findViewById(R.id.thai_sub_distinct);
        spinnerEducation = (Spinner) findViewById(R.id.education);
        spinnerJob = (Spinner) findViewById(R.id.job);
        spinnerPregnancy = (Spinner) findViewById(R.id.pregnancy);

        spinnerProvince.setOnItemSelectedListener(this);
        spinnerDistinct.setOnItemSelectedListener(this);
        spinnerSubDistinct.setOnItemSelectedListener(this);
        spinnerEducation.setOnItemSelectedListener(this);
        spinnerJob.setOnItemSelectedListener(this);
        spinnerPregnancy.setOnItemSelectedListener(this);

        readFileJSON = new ReadFileJSON(this);

        mProvince.clear();
        changwatsPid = readFileJSON.getProvince(SELECT, mProvince);

        readFileJSON.sort(mProvince);
        ArrayAdapter<String> adapterProvince = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, mProvince);
        spinnerProvince.setAdapter(adapterProvince);

        listEducation();
        listJob();
        listPregnancy();
    }

    private void listPregnancy() {

        for (int i = 1; i <= 10; i++) {
            mPregnancy.add(i);
        }

        ArrayAdapter<Integer> adapterProvince = new ArrayAdapter<Integer>(this,
                android.R.layout.simple_dropdown_item_1line, mPregnancy);
        spinnerPregnancy.setAdapter(adapterProvince);
    }

    private void listEducation() {

        mEducation.add(SELECT);
        mEducation.add("ประถมศึกษา");
        mEducation.add("มัธยมศึกษาตอนต้น");
        mEducation.add("มัธยมศึกษาตอนปลาย");
        mEducation.add("ปริญญาตรี");
        mEducation.add("สูงกว่าปริญญาตรี");

        ArrayAdapter<String> adapterProvince = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, mEducation);
        spinnerEducation.setAdapter(adapterProvince);
    }

    private void listJob() {

        mJob.add(SELECT);
        mJob.add("ทำสวน/ทำนา/ทำไร่/เลี้ยงสัตว์ ");
        mJob.add("รับจ้างทั่วไป");
        mJob.add("รับราชการ/รัฐวิสาหกิจ");
        mJob.add("ทำงานโรงงาน/บริษัทเอกชน");
        mJob.add("ค้าขาย/ทำธุรกิจส่วนตัว");
        mJob.add("ทำงานบ้าน/แม่บ้าน");
        mJob.add("ไม่มีงานทำ/ว่างงาน");
        mJob.add("อื่นๆ");

        ArrayAdapter<String> adapterProvince = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, mJob);
        spinnerJob.setAdapter(adapterProvince);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.thai_province:
                changwats = mProvince.get(position);
                mProvince.clear();
                changwatsPid = readFileJSON.getProvince(changwats, mProvince);
                readFileJSON.sort(mProvince);
                mDistinct.clear();
                amphoesPid = readFileJSON.getDistinct(null, mDistinct, changwatsPid);
                readFileJSON.sort(mDistinct);
                ArrayAdapter<String> adapterDistinct = new ArrayAdapter<String>(this,
                        android.R.layout.simple_dropdown_item_1line, mDistinct);
                spinnerDistinct.setAdapter(adapterDistinct);
                break;
            case R.id.thai_distinct:
                amphoes = mDistinct.get(position);
                mDistinct.clear();
                amphoesPid = readFileJSON.getDistinct(amphoes, mDistinct, changwatsPid);
                readFileJSON.sort(mDistinct);
                mSubDistinct.clear();
                readFileJSON.getSubDistinct(mSubDistinct, amphoesPid);

                readFileJSON.sort(mSubDistinct);
                ArrayAdapter<String> adapterThai = new ArrayAdapter<String>(this,
                        android.R.layout.simple_dropdown_item_1line, mSubDistinct);
                spinnerSubDistinct.setAdapter(adapterThai);
                break;
            case R.id.thai_sub_distinct:
                tambons = mSubDistinct.get(position);
                break;
            case R.id.education:
                education = mEducation.get(position);
                otherEducation(position);
                break;
            case R.id.job:
                job = mJob.get(position);
                otherJob(position);
                break;
            case R.id.pregnancy:
                pregnancy = String.valueOf(mPregnancy.get(position));
                onCreateBirthRecord(mPregnancy.get(position));
                break;

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_sign_up:
                submit();
                break;
            case R.id.rbt_congenital_disorder_n:
                congenitalHide();
                break;
            case R.id.rbt_congenital_disorder_y:
                congenitalShow();
                break;
            case R.id.rbt_sickness_n:
                sicknessHide();
                break;
            case R.id.rbt_sickness_y:
                sicknessShow();
                break;
            case R.id.btn_edit_date:
                pickDate(R.id.edt_date);
                break;
            case R.id.btn_edit_gestational_age:
                pickDate(R.id.edt_gestational_age);
                break;
            case R.id.rbt_religion_other:
                otherReligionShow();
                break;
            case R.id.cb_edit_congenital_disorder:
                congenitalOther();
                break;

        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        switch (group.getId()) {
            case R.id.religion:
                otherReligionHide();
                break;
        }
    }

    private void onCreateBirthRecord(int size){

        recyclerView(size);
    }

    private List<indexBirthRecord> initIndex(int size) {

        dataset.clear();
        for (int i = 0; i < size; i++) {
            indexBirthRecord indexBirthRecord = new indexBirthRecord(i);
            dataset.add(indexBirthRecord);
        }
        birthRecords = new String[size];

        return dataset;
    }

    private void recyclerView(int size) {
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        TextView tvFirst = (TextView) findViewById(R.id.birth_record_first);


        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new IndexBirthRecordAdapter(this, initIndex(size));
        mRecyclerView.setAdapter(mAdapter);

        if(size == 1){
            mRecyclerView.setVisibility(View.GONE);
            tvFirst.setVisibility(View.VISIBLE);
            birthRecords[0] = "ท้องแรก";
        }else {
            mRecyclerView.setVisibility(View.VISIBLE);
            tvFirst.setVisibility(View.GONE);
        }

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this,
                mRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
            }

            @Override
            public void onItemLongClick(View view, int position) {
            }
        }));
    }

    private void congenitalOther() {
        CheckBox checkBox = (CheckBox) findViewById(R.id.cb_edit_congenital_disorder);
        TextInputLayout textInputLayout = (TextInputLayout) findViewById(R.id.lay_edt_congenital_disorder);
        EditText edtCongenital = (EditText) findViewById(R.id.edt_congenital_disorder_edit);

        if(checkBox.isChecked()){
            textInputLayout.setVisibility(View.VISIBLE);
        }else{
            textInputLayout.setVisibility(View.GONE);
        }
        edtCongenital.setText("");
    }

    private void otherEducation(int position) {
        TextInputLayout layoutEducation = (TextInputLayout) findViewById(R.id.input_education);
        if (mEducation.get(position).equals("อื่นๆ")) {
            layoutEducation.setVisibility(View.VISIBLE);
        } else {
            layoutEducation.setVisibility(View.GONE);
        }
    }

    private void otherJob(int position) {
        TextInputLayout layoutJob = (TextInputLayout) findViewById(R.id.input_job);
        if (mJob.get(position).equals("อื่นๆ")) {
            layoutJob.setVisibility(View.VISIBLE);
        } else {
            layoutJob.setVisibility(View.GONE);
        }
    }

    private void otherReligionShow() {
        try {
            RadioGroup radioGroup = (RadioGroup) findViewById(R.id.religion);
            int radioButtonID = radioGroup.getCheckedRadioButtonId();
            RadioButton radioButton = (RadioButton) radioGroup.findViewById(radioButtonID);
            radioButton.setChecked(false);
        } catch (Exception e) {
        }
        RadioButton radioButtonOther = (RadioButton) findViewById(R.id.rbt_religion_other);
        radioButtonOther.setChecked(true);
        TextInputLayout textInputLayout = (TextInputLayout) findViewById(R.id.input_religion);
        textInputLayout.setVisibility(View.VISIBLE);
    }

    private void otherReligionHide() {
        try {

            RadioButton radioButtonOther = (RadioButton) findViewById(R.id.rbt_religion_other);
            radioButtonOther.setChecked(false);

            TextInputLayout textInputLayout = (TextInputLayout) findViewById(R.id.input_religion);
            textInputLayout.setVisibility(View.GONE);
        } catch (Exception e) {

        }
    }

    private void pickDate(int viewId) {
        DialogFragment newFragment = new DatePickerFragment(getApplicationContext(), activity, viewId);
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    private void congenitalHide() {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.lay_congenital_disorder);
        linearLayout.setVisibility(View.GONE);
    }

    private void congenitalShow() {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.lay_congenital_disorder);
        linearLayout.setVisibility(View.VISIBLE);
    }

    private void sicknessHide() {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.lay_sickness);
        linearLayout.setVisibility(View.GONE);
    }

    private void sicknessShow() {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.lay_sickness);
        linearLayout.setVisibility(View.VISIBLE);
    }


    private void submit() {
        EditText edtName = (EditText) findViewById(R.id.edt_name);
        EditText edtLastName = (EditText) findViewById(R.id.edt_lastname);
        EditText edtTel = (EditText) findViewById(R.id.edt_tel);
        EditText edtDate = (EditText) findViewById(R.id.edt_date);
        EditText edtGestational = (EditText) findViewById(R.id.edt_gestational_age);
        EditText edtPregnancy = (EditText) findViewById(R.id.edt_pregnancy);
        EditText edtEmail = (EditText) findViewById(R.id.edt_email);
        EditText edtLine = (EditText) findViewById(R.id.edt_line);
        EditText edtSalary = (EditText) findViewById(R.id.edt_salary);
        EditText edtEducationOther = (EditText) findViewById(R.id.edt_education);
        EditText edtJobOther = (EditText) findViewById(R.id.edt_job);

        try {
            if (!isEmailValid(edtEmail.getText().toString())) {
                showToast("Your Email is Invalid.");
            } else if (
                    edtName.getText().toString().isEmpty() ||
                            edtLastName.getText().toString().isEmpty() ||
                            edtTel.getText().toString().isEmpty() ||
                            edtDate.getText().toString().isEmpty() ||
                            edtGestational.getText().toString().isEmpty() ||
                            edtEmail.getText().toString().isEmpty() ||
                            edtLine.getText().toString().isEmpty() ||
                            edtSalary.getText().toString().isEmpty()
                    ) {

                showToast("All fields are required.");
                congenital = "";



            } else {

                name = edtName.getText().toString();
                lastName = edtLastName.getText().toString();
                tel = edtTel.getText().toString();
                gestational = edtGestational.getText().toString();
                birthDate = edtDate.getText().toString();
                email = edtEmail.getText().toString();
                line = edtLine.getText().toString();
                salary = edtSalary.getText().toString();

                if (!edtEducationOther.getText().toString().isEmpty()) {
                    education = edtEducationOther.getText().toString();
                }

                if (!edtJobOther.getText().toString().isEmpty()) {
                    job = edtJobOther.getText().toString();
                }

                getCongenital();
                getSickness();
                getBirthRecord();
                getReligion();


                birthRecord = "";
                for (int i = 0;i < birthRecords.length;i++){
                    birthRecord += birthRecords[i] + ",";
                }
                birthRecord = birthRecord.substring(0,birthRecord.length() - 1);
//                Log.d("birthRecord",birthRecord);

                done();


            }
        } catch (Exception e) {
            showToast("All fields are required.");
            congenital = "";
        }


    }

    private void showToast(String test) {
        Toast.makeText(getApplicationContext(), test,
                Toast.LENGTH_SHORT).show();
    }

    private void getCongenital() {
        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.congenital_disorder);
        int radioButtonID = radioGroup.getCheckedRadioButtonId();

        RadioButton radioButton = (RadioButton) radioGroup.findViewById(radioButtonID);
        if (radioButton.getText().toString().equals("มีโรคประจำตัว")) {
            CheckBox item1 = (CheckBox) findViewById(R.id.cb_item_1);
            CheckBox item2 = (CheckBox) findViewById(R.id.cb_item_2);
            CheckBox item3 = (CheckBox) findViewById(R.id.cb_item_3);
            CheckBox item4 = (CheckBox) findViewById(R.id.cb_item_4);
            CheckBox item5 = (CheckBox) findViewById(R.id.cb_item_5);
            CheckBox item6 = (CheckBox) findViewById(R.id.cb_item_6);

            insertCongenital(item1);
            insertCongenital(item2);
            insertCongenital(item3);
            insertCongenital(item4);
            insertCongenital(item5);
            insertCongenital(item6);

            EditText edtCongenital = (EditText) findViewById(R.id.edt_congenital_disorder);

            if (!edtCongenital.getText().toString().isEmpty()) {
                congenital += edtCongenital.getText().toString() + ", ";
            }
        } else {
            congenital = radioButton.getText().toString() + ", ";


        }

    }

    private void insertCongenital(CheckBox checkBox) {
        if (checkBox.isChecked()) {
            congenital += checkBox.getText().toString() + ", ";
        }
    }

    private void getSickness() {
        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.sickness);
        int radioButtonID = radioGroup.getCheckedRadioButtonId();

        RadioButton radioButton = (RadioButton) radioGroup.findViewById(radioButtonID);
        if (radioButton.getText().toString().equals("มี")) {

            EditText edtSickness = (EditText) findViewById(R.id.edt_sickness);

            if (!edtSickness.getText().toString().isEmpty()) {
                sickness = edtSickness.getText().toString();
            }
        } else {
            sickness = radioButton.getText().toString();

        }

    }

    /** Get data in array form adapter */
    private void getBirthRecord() {
        birthRecord = "";
        for (int i = 0;i < birthRecords.length;i++){
            birthRecord += birthRecords[i] + ",";
        }
        birthRecord = birthRecord.substring(0,birthRecord.length() - 1);
    }

    private void getReligion() {
        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.religion);
        int radioButtonID = radioGroup.getCheckedRadioButtonId();

        RadioButton radioButton = (RadioButton) radioGroup.findViewById(radioButtonID);

        religion = radioButton.getText().toString();

        RadioButton radioButtonOther = (RadioButton) findViewById(R.id.rbt_religion_other);
        if (radioButtonOther.isChecked()) {
            EditText editText = (EditText) findViewById(R.id.edt_religion);
            religion = editText.getText().toString();
        }
    }

    private void done() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setMessage("You want to submit.");
        alert.setCancelable(false);
        alert.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                saveEditDone();
            }
        });
        alert.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alert.show();
    }

    private void saveEditDone() {
        showProgressDialog();

//        getKey();

        /** Update information */

        String UID = FirebaseAuth.getInstance().getCurrentUser().getUid();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference(UID).child("information");
        final DatabaseReference role = database.getReference(UID).child("role");

        myRef.child("id").setValue(key);
        myRef.child("name").setValue(name);
        myRef.child("lastname").setValue(lastName);
        myRef.child("birthdate").setValue(birthDate);
        myRef.child("tel").setValue(tel);
        myRef.child("email").setValue(email);
        myRef.child("line").setValue(line);
        myRef.child("address").child("province").setValue(changwats);
        myRef.child("address").child("distinct").setValue(amphoes);
        myRef.child("address").child("sub distinct").setValue(tambons);
        myRef.child("sickness").setValue(sickness);
        myRef.child("congenital").setValue(congenital.substring(0, congenital.length() - 2));
        myRef.child("pregnancy").setValue(pregnancy);
        myRef.child("gestational").setValue(gestational);
        myRef.child("birthRecord").setValue(birthRecord);
        myRef.child("job").setValue(job);
        myRef.child("education").setValue(education);
        myRef.child("salary").setValue(salary);
        myRef.child("religion").setValue(religion);
        role.setValue("User");

        StorageReference mStorageRef = FirebaseStorage.getInstance().getReference();
        StorageReference imageRef = mStorageRef.child("profile.JPEG");
        imageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                String url = uri.toString();
                myRef.child("image").setValue(url);

            }
        });

        updateKey();

        hideProgressDialog();

        goTo(MainActivity.class);
    }

    private void getKey() {

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("key");

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                key = dataSnapshot.getValue(String.class);
                Log.d("key", key);
            }

            @Override
            public void onCancelled(DatabaseError error) {
            }
        });

    }

    private void updateKey() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference keyRef = database.getReference("key");
        String str = key.substring(4, key.length());
//        Log.d("key",str);
        int i = Integer.parseInt(str);
        i++;
        String updateKey = null;
        if (String.valueOf(i).length() == 1) {
            updateKey = "SM 00" + i;
        } else if (String.valueOf(i).length() == 2) {
            updateKey = "SM 0" + i;
        } else if (String.valueOf(i).length() == 3) {
            updateKey = "SM " + i;
        }
        keyRef.setValue(updateKey);


    }

    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }


//    private boolean isValidate() {
//        if (id.length() != 13) {
//            Toast.makeText(getApplicationContext(), "Your Id is Invalid .",
//                    Toast.LENGTH_SHORT).show();
//            return false;
//        } else {
//            return true;
//        }
//    }


}
