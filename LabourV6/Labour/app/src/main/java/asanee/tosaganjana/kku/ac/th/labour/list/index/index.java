package asanee.tosaganjana.kku.ac.th.labour.list.index;

/**
 * Created by Puppymind on 29/4/2560.
 */

public class index {
    private String title;
    private int index;
    private String detail;

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public index(int index, String title, String detail) {
        this.title = title;
        this.index = index;
        this.detail = detail;
    }

    public String getTitle() {

        return title;
    }

    public void setTitle(String title) {

        this.title = title;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}