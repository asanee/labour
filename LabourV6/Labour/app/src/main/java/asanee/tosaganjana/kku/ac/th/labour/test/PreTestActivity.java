package asanee.tosaganjana.kku.ac.th.labour.test;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;

import asanee.tosaganjana.kku.ac.th.labour.R;
import asanee.tosaganjana.kku.ac.th.labour.fonts.CustomButton;
import asanee.tosaganjana.kku.ac.th.labour.fonts.CustomRadioButton;
import asanee.tosaganjana.kku.ac.th.labour.fonts.CustomTextView;
import asanee.tosaganjana.kku.ac.th.labour.main.BaseActivity;
import asanee.tosaganjana.kku.ac.th.labour.main.MainActivity;


public class PreTestActivity extends BaseActivity implements View.OnClickListener {

    private StringBuilder builder;
    private Button submit, next;
    private TextView problem, text_part;
    private int scorePart1 = 0, count = 0, mode = 1,scorePart2 = 0,numPart1 = 0 ,numPart2 = 0, numPart3 = 0 ,scorePart3 = 0;
    private RadioGroup group;
    private RadioButton radio1, radio2, radio3, radio4, radio5;
    private JSONObject exam;
    private String part;
    private String ans;
    private JSONArray partArray;
    private ArrayList ansPart1 = new ArrayList();
    private ArrayList ansPart2 = new ArrayList();
    private ArrayList ansPart3 = new ArrayList();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_test);

        part = "ทัศนคติเกี่ยวกับการตั้งครรภ์";
        init();
        builder();

    }

    private void builder() {
        Resources res = getResources();
        InputStream is = res.openRawResource(R.raw.pretest);
        Scanner scanner = new Scanner(is);

        builder = new StringBuilder();

        while (scanner.hasNextLine()) {
            builder.append(scanner.nextLine());
        }
        readFile(builder.toString(), count);
    }

    @Override
    public void onBackPressed() {
        goBack(MainActivity.class);
    }

    private void init() {

        problem = (CustomTextView) findViewById(R.id.problem);
        text_part = (CustomTextView) findViewById(R.id.part);
        group = (RadioGroup) findViewById(R.id.radio_group);
        radio1 = (CustomRadioButton) findViewById(R.id.radio1);
        radio2 = (CustomRadioButton) findViewById(R.id.radio2);
        radio3 = (CustomRadioButton) findViewById(R.id.radio3);
        radio4 = (CustomRadioButton) findViewById(R.id.radio4);
        radio5 = (CustomRadioButton) findViewById(R.id.radio5);
        submit = (CustomButton) findViewById(R.id.submit);
        next = (CustomButton) findViewById(R.id.next);

        next.setOnClickListener(this);
        submit.setOnClickListener(this);

    }

    public void readFile(String s, int i) {
        try {

            submit.setVisibility(View.INVISIBLE);
            group.clearCheck();
            JSONObject root = new JSONObject(s);
            exam =  root.getJSONObject("preTest2");
            partArray = (JSONArray) exam.get(part);

            if(part == "ทัศนคติเกี่ยวกับการตั้งครรภ์") {
                String question = partArray.getJSONObject(i).getString("problem" + String.valueOf(i + 1));
                String choice1 = partArray.getJSONObject(i).getString("choice1");
                String choice2 = partArray.getJSONObject(i).getString("choice2");
                String choice3 = partArray.getJSONObject(i).getString("choice3");
                String choice4 = partArray.getJSONObject(i).getString("choice4");
                String choice5 = partArray.getJSONObject(i).getString("choice5");
                //ans = partArray.getJSONObject(i).getString("ans");

                text_part.setText(part);
                problem.setText(question);
                radio1.setText("A. " + choice1);
                radio2.setText("B. " + choice2);
                radio3.setText("C. " + choice3);
                radio4.setText("D. " + choice4);
                radio5.setText("E. " + choice5);

            }else if( part == "ความรู้ทั่วไปของหญิงตั้งครรภ์ไตรมาสที่ 3"){
                String question = partArray.getJSONObject(i).getString("problem" + String.valueOf(i + 1));
                String choice1 = partArray.getJSONObject(i).getString("choice1");
                String choice2 = partArray.getJSONObject(i).getString("choice2");
                String choice3 = partArray.getJSONObject(i).getString("choice3");

                text_part.setText(part);
                problem.setText(question);
                radio1.setText("A. " + choice1);
                radio2.setText("B. " + choice2);
                radio3.setText("C." + choice3);

            }else if(part == "พฤติกรรมสุขภาพของหญิงตั้งครรภ์ในไตรมาสที่ 3") {
                String question = partArray.getJSONObject(i).getString("problem" + String.valueOf(i + 1));
                String choice1 = partArray.getJSONObject(i).getString("choice1");
                String choice2 = partArray.getJSONObject(i).getString("choice2");
                ans = partArray.getJSONObject(i).getString("ans");

                text_part.setText(part);
                problem.setText(question);
                radio1.setText("A. " + choice1);
                radio2.setText("B. " + choice2);
            }

        } catch (JSONException e) {

        }
    }

    //checkAnsPart1
    public void checkAnsPart1(int i) {

        if (i == 1) {
            ansPart1.add(radio1.getText().toString());
            if (count + 1 == partArray.length()) {
                radio1.setChecked(true);
                radio2.setEnabled(false);
                radio3.setEnabled(false);
                radio4.setEnabled(false);
                radio5.setEnabled(false);
                //submit.setText(String.valueOf(score));
            }
            if(count >= 5 && count <= 8) {
                scorePart1 += 5;
            }else
                scorePart1 += 1;
        } else if (i == 2) {
            ansPart1.add(radio2.getText().toString());
            if (count + 1 == partArray.length()) {
                radio2.setChecked(true);
                radio1.setEnabled(false);
                radio3.setEnabled(false);
                radio4.setEnabled(false);
                radio5.setEnabled(false);
                //submit.setText(String.valueOf(score));
            }
            if(count >= 5 && count <= 8) {
                scorePart1 += 4;
            }else
                scorePart1 += 2;
        } else if( i== 3){
            ansPart1.add(radio3.getText().toString());
            if (count + 1 == partArray.length()) {
                radio2.setEnabled(false);
                radio1.setEnabled(false);
                radio3.setChecked(true);
                radio4.setEnabled(false);
                radio5.setEnabled(false);

            }
            if(count >= 5 && count <= 8) {
                scorePart1 += 3;
            }else
                scorePart1 += 3;

        }else if( i== 4){
            ansPart1.add(radio4.getText().toString());
            if (count + 1 == partArray.length()) {
                radio2.setEnabled(false);
                radio1.setEnabled(false);
                radio3.setEnabled(false);
                radio4.setChecked(true);
                radio5.setEnabled(false);

            }
            if(count >= 5 && count <= 8) {
                scorePart1 += 2;
            }else
                scorePart1 += 4;

        }else if( i == 5){
            ansPart1.add(radio5.getText().toString());
            if (count + 1 == partArray.length()) {
                radio2.setEnabled(false);
                radio1.setEnabled(false);
                radio3.setEnabled(false);
                radio4.setEnabled(false);
                radio5.setChecked(true);
                //submit.setText(String.valueOf(score));
            }
            if(count >= 5 && count <= 8) {
                scorePart1 += 1;
            }else
                scorePart1 += 5;

        }

//        next.setText(String.valueOf(scorePart1));
        numPart1 ++;
    }
    public void checkAnsPart2(int i){
        if (i == 1) {
            ansPart2.add(radio1.getText().toString());
            if (count + 1 == partArray.length()) {
                radio1.setChecked(true);
                radio2.setEnabled(false);
                radio3.setEnabled(false);
            }
            scorePart2 += 1;
        } else if (i == 2) {
            ansPart2.add(radio2.getText().toString());
            if (count + 1 == partArray.length()) {
                radio2.setChecked(true);
                radio1.setEnabled(false);
                radio3.setEnabled(false);
            }
            scorePart2 +=0;
        } else if( i== 3){
            ansPart2.add(radio3.getText().toString());
            if (count + 1 == partArray.length()) {
                radio2.setEnabled(false);
                radio1.setEnabled(false);
                radio3.setChecked(true);

            }
            scorePart2 +=0;
        }
//        next.setText(String.valueOf(scorePart2));
        numPart2 ++;
    }
    public void checkAnsPart3(int i){
        if (i == 1) {
            ansPart3.add(radio1.getText().toString());
            if (count + 1 == partArray.length()) {
                radio1.setChecked(true);
                radio2.setEnabled(false);
                //submit.setText(String.valueOf(score));
            }
            if(i == Integer.valueOf(ans)) {
                scorePart3 += 1;
            }
        } else if (i == 2) {
            ansPart3.add(radio2.getText().toString());
            if (count + 1 == partArray.length()) {
                radio2.setChecked(true);
                radio1.setEnabled(false);
                //submit.setText(String.valueOf(score));
            }
            if(i == Integer.valueOf(ans)) {
                scorePart3 += 1;
            }
        }

//        next.setText(String.valueOf(scorePart3));
        numPart3++;
    }
    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.submit:
                if(mode == 2){
                    count = 0;
                    part = "ความรู้ทั่วไปของหญิงตั้งครรภ์ไตรมาสที่ 3";
                    radio1.setEnabled(true);
                    radio2.setEnabled(true);
                    radio3.setEnabled(true);
                    radio4.setVisibility(View.GONE);
                    radio5.setVisibility(View.GONE);
                    next.setVisibility(View.VISIBLE);
                    readFile(builder.toString(), count);
                } else if(mode == 3){
                    count = 0;
                    part = "พฤติกรรมสุขภาพของหญิงตั้งครรภ์ในไตรมาสที่ 3";
                    radio1.setEnabled(true);
                    radio2.setEnabled(true);
                    radio3.setVisibility(View.GONE);
                    next.setVisibility(View.VISIBLE);
                    readFile(builder.toString(), count);
                }
                else if(mode == 0){

                    String testType = getIntent().getExtras().getString("test_type");

                    Intent intent = new Intent(this,ResultPreTest.class);
                    intent.putExtra("scorePart1",scorePart1);
                    intent.putExtra("scorePart2",scorePart2);
                    intent.putExtra("scorePart3",scorePart3);
                    intent.putExtra("ansPart1",getAnswer(ansPart1));
                    intent.putExtra("ansPart2",getAnswer(ansPart2));
                    intent.putExtra("ansPart3",getAnswer(ansPart3));
                    intent.putExtra("test_type",testType);
                    startActivity(intent);

                }
                break;
            case R.id.next:
                next();
                break;
        }
    }

    private String getAnswer(ArrayList item){
        String ans = "";
        for (int i = 0;i < item.size() ;i++){
            ans += item.get(i) + "," ;
        }
        ans = ans.substring(0,ans.length()-1);
        ans = ans.replaceAll("\\s+","");

        return ans;
    }

    private void next() {

        if (radio1.isChecked() || radio2.isChecked() || radio3.isChecked() || radio4.isChecked() || radio5.isChecked() ) {
            if(mode == 1) {
                if (radio1.isChecked()) {
                    checkAnsPart1(1);
                } else if (radio2.isChecked()) {
                    checkAnsPart1(2);
                } else if (radio3.isChecked()) {
                    checkAnsPart1(3);
                } else if (radio4.isChecked()) {
                    checkAnsPart1(4);
                } else if (radio5.isChecked()) {
                    checkAnsPart1(5);
                }
            }else if(mode == 2){
                if (radio1.isChecked()) {
                    checkAnsPart2(1);
                } else if (radio2.isChecked()) {
                    checkAnsPart2(2);
                } else if (radio3.isChecked()) {
                    checkAnsPart2(3);
                }
            }else if(mode == 3){
                if (radio1.isChecked()) {
                    checkAnsPart3(1);
                } else if (radio2.isChecked()) {
                    checkAnsPart3(2);
                }
            }
            count++;
            if (count == partArray.length()) { // last number will show button submit and hide button next

                submit.setVisibility(View.VISIBLE);
                next.setVisibility(View.INVISIBLE);
                if(mode == 1) {
                    mode = 2;
                    submit.setText("ส่วนที่ 2");
                }else if(mode == 2){
                    mode = 3;
                    submit.setText("ส่วนที่ 3");
                }else if(mode == 3){
                    mode = 0;
                    submit.setText("เรียบร้อย");
                }
            } else {
                readFile(builder.toString(), count);
            }

        } else
            Toast.makeText(PreTestActivity.this, "Please select answer", Toast.LENGTH_LONG).show();
    }

}

