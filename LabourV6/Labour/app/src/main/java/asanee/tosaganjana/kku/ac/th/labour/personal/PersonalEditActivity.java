package asanee.tosaganjana.kku.ac.th.labour.personal;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import asanee.tosaganjana.kku.ac.th.labour.R;
import asanee.tosaganjana.kku.ac.th.labour.list.index.IndexBirthRecordAdapter;
import asanee.tosaganjana.kku.ac.th.labour.list.index.RecyclerItemClickListener;
import asanee.tosaganjana.kku.ac.th.labour.list.index.indexBirthRecord;
import asanee.tosaganjana.kku.ac.th.labour.main.date.DatePickerFragment;
import asanee.tosaganjana.kku.ac.th.labour.main.signup.ReadFileJSON;

import static android.content.ContentValues.TAG;
import static asanee.tosaganjana.kku.ac.th.labour.main.signup.SignUpActivity.birthRecords;

public class PersonalEditActivity extends PersonalActivity
        implements View.OnClickListener, AdapterView.OnItemSelectedListener, RadioGroup.OnCheckedChangeListener {

    private static final int PICK_PHOTO_FOR_AVATAR = 1000;
    ImageView imageView;
    private Spinner spinnerProvince, spinnerDistinct, spinnerSubDistinct, spinnerEducation, spinnerJob, spinnerPregnancy;
    private ArrayList<String> mProvince = new ArrayList<String>();
    private ArrayList<String> mDistinct = new ArrayList<String>();
    private ArrayList<String> mSubDistinct = new ArrayList<String>();
    private ArrayList<String> mEducation = new ArrayList<String>();
    private ArrayList<String> mJob = new ArrayList<String>();
    private ArrayList<Integer> mPregnancy = new ArrayList<Integer>();

    private String changwats, amphoes, tambons, education, job;
    private String tmpProvice, tmpDistinct, tmpSubDistinct;
    private String changwatsPid, amphoesPid;
    private final static String SELECT = "-- pleases select --";
    private Context mContext;
    private Activity activity;
    private String id, name, lastName, tel, birthDate, salary, birthRecord;
    private String gestational, pregnancy, sickness, congenital, religion;
    private ReadFileJSON readFileJSON;
    private boolean isCongenital = false, isSickness = false ,isPregnancy = false;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    List<indexBirthRecord> dataset = new ArrayList<indexBirthRecord>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_edit);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle("แก้ไขข้อมูลส่วนตัว");


        init();
        initDrawerLayout(null);
        upDateUI();
        spinnerUpdate();
    }

    private void spinnerUpdate() {

        spinnerProvince = (Spinner) findViewById(R.id.thai_province);
        spinnerDistinct = (Spinner) findViewById(R.id.thai_distinct);
        spinnerSubDistinct = (Spinner) findViewById(R.id.thai_sub_distinct);
        spinnerEducation = (Spinner) findViewById(R.id.education);
        spinnerJob = (Spinner) findViewById(R.id.job);
        spinnerPregnancy = (Spinner) findViewById(R.id.pregnancy);

        spinnerProvince.setOnItemSelectedListener(this);
        spinnerDistinct.setOnItemSelectedListener(this);
        spinnerSubDistinct.setOnItemSelectedListener(this);
        spinnerEducation.setOnItemSelectedListener(this);
        spinnerJob.setOnItemSelectedListener(this);
        spinnerPregnancy.setOnItemSelectedListener(this);

        readFileJSON = new ReadFileJSON(this);

        mProvince.clear();
        changwatsPid = readFileJSON.getProvince(SELECT, mProvince);

        readFileJSON.sort(mProvince);
        ArrayAdapter<String> adapterProvince = new ArrayAdapter<String>(mContext,
                android.R.layout.simple_dropdown_item_1line, mProvince);
        spinnerProvince.setAdapter(adapterProvince);

    }

    private void listEducation() {

        mEducation.add(SELECT);
        mEducation.add("ประถมศึกษา");
        mEducation.add("มัธยมศึกษาตอนต้น");
        mEducation.add("มัธยมศึกษาตอนปลาย");
        mEducation.add("ปริญญาตรี");
        mEducation.add("สูงกว่าปริญญาตรี");

        ArrayAdapter<String> adapterProvince = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, mEducation);
        spinnerEducation.setAdapter(adapterProvince);
    }

    private void listJob() {

        mJob.add(SELECT);
        mJob.add("ทำสวน/ทำนา/ทำไร่/เลี้ยงสัตว์ ");
        mJob.add("รับจ้างทั่วไป");
        mJob.add("รับราชการ/รัฐวิสาหกิจ");
        mJob.add("ทำงานโรงงาน/บริษัทเอกชน");
        mJob.add("ค้าขาย/ทำธุรกิจส่วนตัว");
        mJob.add("ทำงานบ้าน/แม่บ้าน");
        mJob.add("ไม่มีงานทำ/ว่างงาน");
        mJob.add("อื่นๆ");

        ArrayAdapter<String> adapterProvince = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, mJob);
        spinnerJob.setAdapter(adapterProvince);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        switch (parent.getId()) {
            case R.id.thai_province:
                changwats = mProvince.get(position);
                mProvince.clear();
                changwatsPid = readFileJSON.getProvince(changwats, mProvince);
                readFileJSON.sort(mProvince);
                mDistinct.clear();
                amphoesPid = readFileJSON.getDistinct(null, mDistinct, changwatsPid);
                readFileJSON.sort(mDistinct);
                ArrayAdapter<String> adapterDistinct = new ArrayAdapter<String>(mContext,
                        android.R.layout.simple_dropdown_item_1line, mDistinct);
                spinnerDistinct.setAdapter(adapterDistinct);
                break;
            case R.id.thai_distinct:
                amphoes = mDistinct.get(position);
                mDistinct.clear();
                amphoesPid = readFileJSON.getDistinct(amphoes, mDistinct, changwatsPid);
                readFileJSON.sort(mDistinct);
                mSubDistinct.clear();
                readFileJSON.getSubDistinct(mSubDistinct, amphoesPid);

                readFileJSON.sort(mSubDistinct);
                ArrayAdapter<String> adapterThai = new ArrayAdapter<String>(PersonalEditActivity.this,
                        android.R.layout.simple_dropdown_item_1line, mSubDistinct);
                spinnerSubDistinct.setAdapter(adapterThai);
                break;
            case R.id.thai_sub_distinct:
                tambons = mSubDistinct.get(position);
                break;
            case R.id.education:
                education = mEducation.get(position);
                otherEducation(position);
                break;
            case R.id.job:
                job = mJob.get(position);
                otherJob(position);
                break;
            case R.id.pregnancy:
                pregnancy = String.valueOf(mPregnancy.get(position));
                onCreateBirthRecord(mPregnancy.get(position));
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.bgheader:
                pickImage();
                break;
            case R.id.fab_done:
                done();
                break;
            case R.id.btn_edit_date:
                datePicker(R.id.edt_date);
                break;
            case R.id.btn_edit_gestational_age:
                datePicker(R.id.edt_gestational_age);
                break;
            case R.id.btn_edit_address:
                editLayout(R.id.address, R.id.edit_address);
                tmpProvice = changwats;
                tmpDistinct = amphoes;
                tmpSubDistinct = tambons;
                break;
            case R.id.btn_edit_address_cancel:
                cancelLayout(R.id.address, R.id.edit_address);
                changwats = tmpProvice;
                amphoes = tmpDistinct;
                tambons = tmpSubDistinct;
                break;
            case R.id.btn_congenital_disorder:
                editLayout(R.id.layout_congenital_disorder, R.id.layout_congenital_disorder_edit);
                isCongenital = true;
                break;
            case R.id.btn_edit_congenital_disorder_cancel:
                cancelLayout(R.id.layout_congenital_disorder, R.id.layout_congenital_disorder_edit);
                isCongenital = false;
                break;
            case R.id.back:
                back();
                break;
            case R.id.rbt_congenital_disorder_n:
                congenitalHide();
                break;
            case R.id.rbt_congenital_disorder_y:
                congenitalShow();
                break;
            case R.id.btn_sickness:
                editLayout(R.id.layout_sickness, R.id.layout_sickness_edit);
                isSickness = true;
                break;
            case R.id.btn_edit_sickness_cancel:
                cancelLayout(R.id.layout_sickness, R.id.layout_sickness_edit);
                isSickness = false;
                break;
            case R.id.rbt_sickness_n:
                sicknessHide();
                break;
            case R.id.rbt_sickness_y:
                sicknessShow();
                break;
            case R.id.rbt_religion_other:
                otherReligionShow();
                break;
            case R.id.btn_edit_pregnancy:
                editLayout(R.id.lay_pregnancy, R.id.edit_lay_pregnancy);
                listPregnancy();
                isPregnancy = true;
                break;
            case R.id.btn_edit_lay_pregnancy_cancel:
                cancelLayout(R.id.lay_pregnancy, R.id.edit_lay_pregnancy);
                isPregnancy = false;
                break;
            case R.id.cb_edit_congenital_disorder:
                congenitalOther();
                break;

        }
    }

    private void init() {

        findViewById(R.id.rbt_congenital_disorder_n).setOnClickListener(this);
        findViewById(R.id.rbt_congenital_disorder_y).setOnClickListener(this);
        findViewById(R.id.btn_edit_date).setOnClickListener(this);
        findViewById(R.id.btn_edit_gestational_age).setOnClickListener(this);
        findViewById(R.id.btn_edit_address).setOnClickListener(this);
        findViewById(R.id.btn_edit_address_cancel).setOnClickListener(this);
        findViewById(R.id.back).setOnClickListener(this);
        findViewById(R.id.fab_done).setOnClickListener(this);
        findViewById(R.id.btn_congenital_disorder).setOnClickListener(this);
        findViewById(R.id.btn_edit_congenital_disorder_cancel).setOnClickListener(this);
        findViewById(R.id.btn_edit_sickness_cancel).setOnClickListener(this);
        findViewById(R.id.btn_sickness).setOnClickListener(this);
        findViewById(R.id.rbt_sickness_n).setOnClickListener(this);
        findViewById(R.id.rbt_sickness_y).setOnClickListener(this);
        findViewById(R.id.rbt_religion_other).setOnClickListener(this);
        findViewById(R.id.btn_edit_pregnancy).setOnClickListener(this);
        findViewById(R.id.btn_edit_lay_pregnancy_cancel).setOnClickListener(this);
        findViewById(R.id.cb_edit_congenital_disorder).setOnClickListener(this);


        imageView = (ImageView) findViewById(R.id.bgheader);
        imageView.setOnClickListener(this);

        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.religion);
        radioGroup.setOnCheckedChangeListener(this);

        mContext = this;
        activity = this;
    }

    @Override
    public void back() {
        goBack(PersonalActivity.class);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home) {
            Intent intent = new Intent(getApplicationContext(), PersonalActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    private void done() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setMessage("You want to edit profile.");
        alert.setCancelable(false);
        alert.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                saveEditDone();

            }
        });
        alert.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alert.show();
    }

    private void congenitalOther() {
        CheckBox checkBox = (CheckBox) findViewById(R.id.cb_edit_congenital_disorder);
        TextInputLayout textInputLayout = (TextInputLayout) findViewById(R.id.lay_edt_congenital_disorder);
        EditText edtCongenital = (EditText) findViewById(R.id.edt_congenital_disorder_edit);

        if(checkBox.isChecked()){
            textInputLayout.setVisibility(View.VISIBLE);
        }else{
            textInputLayout.setVisibility(View.GONE);
        }
        edtCongenital.setText("");
    }

    private void congenitalHide() {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.congenital_disorder_checkbox);
        linearLayout.setVisibility(View.GONE);
    }

    private void congenitalShow() {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.congenital_disorder_checkbox);
        linearLayout.setVisibility(View.VISIBLE);
    }

    private void otherEducation(int position) {
        TextInputLayout layoutEducation = (TextInputLayout) findViewById(R.id.input_education);
        if (mEducation.get(position).equals("อื่นๆ")) {
            layoutEducation.setVisibility(View.VISIBLE);
        } else {
            layoutEducation.setVisibility(View.GONE);
        }
    }

    private void otherJob(int position) {
        TextInputLayout layoutJob = (TextInputLayout) findViewById(R.id.input_job);
        if (mJob.get(position).equals("อื่นๆ")) {
            layoutJob.setVisibility(View.VISIBLE);
        } else {
            layoutJob.setVisibility(View.GONE);
        }
    }

    private void getSickness() {
        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.sickness);
        int radioButtonID = radioGroup.getCheckedRadioButtonId();

        RadioButton radioButton = (RadioButton) radioGroup.findViewById(radioButtonID);
        if (radioButton.getText().toString().equals("มี")) {

            EditText edtSickness = (EditText) findViewById(R.id.edt_sickness_edit);

            if (!edtSickness.getText().toString().isEmpty()) {
                sickness = edtSickness.getText().toString();
            }
        } else {
            sickness = radioButton.getText().toString();

        }

    }

    private void sicknessHide() {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.lay_sickness);
        linearLayout.setVisibility(View.GONE);
    }

    private void sicknessShow() {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.lay_sickness);
        linearLayout.setVisibility(View.VISIBLE);
    }


    private void editLayout(int idLayout, int idEditLayout) {
        LinearLayout linearLayout = (LinearLayout) findViewById(idLayout);
        LinearLayout editLinearLayout = (LinearLayout) findViewById(idEditLayout);

        linearLayout.setVisibility(View.GONE);
        editLinearLayout.setVisibility(View.VISIBLE);
    }

    private void cancelLayout(int idLayout, int idEditLayout) {
        LinearLayout linearLayout = (LinearLayout) findViewById(idLayout);
        LinearLayout editLinearLayout = (LinearLayout) findViewById(idEditLayout);

        linearLayout.setVisibility(View.VISIBLE);
        editLinearLayout.setVisibility(View.GONE);
    }


    private void getCongenital() {
        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.congenital_disorder);
        int radioButtonID = radioGroup.getCheckedRadioButtonId();

        RadioButton radioButton = (RadioButton) radioGroup.findViewById(radioButtonID);
        if (radioButton.getText().toString().equals("มีโรคประจำตัว")) {
            CheckBox item1 = (CheckBox) findViewById(R.id.cb_item_1);
            CheckBox item2 = (CheckBox) findViewById(R.id.cb_item_2);
            CheckBox item3 = (CheckBox) findViewById(R.id.cb_item_3);
            CheckBox item4 = (CheckBox) findViewById(R.id.cb_item_4);
            CheckBox item5 = (CheckBox) findViewById(R.id.cb_item_5);
            CheckBox item6 = (CheckBox) findViewById(R.id.cb_item_6);

            insertCongenital(item1);
            insertCongenital(item2);
            insertCongenital(item3);
            insertCongenital(item4);
            insertCongenital(item5);
            insertCongenital(item6);

            EditText edtCongenital = (EditText) findViewById(R.id.edt_congenital_disorder_edit);

            if (!edtCongenital.getText().toString().isEmpty()) {
                congenital += edtCongenital.getText().toString() + ", ";
            }
        } else {
            congenital = radioButton.getText().toString() + ", ";
        }

    }

    private void insertCongenital(CheckBox checkBox) {
        if (checkBox.isChecked()) {
            congenital += checkBox.getText().toString() + ", ";
        }
    }

    public void pickImage() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, PICK_PHOTO_FOR_AVATAR);
    }

    private void getReligion() {
        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.religion);
        int radioButtonID = radioGroup.getCheckedRadioButtonId();

        RadioButton radioButton = (RadioButton) radioGroup.findViewById(radioButtonID);

        religion = radioButton.getText().toString();

        RadioButton radioButtonOther = (RadioButton) findViewById(R.id.rbt_religion_other);
        if (radioButtonOther.isChecked()) {
            EditText editText = (EditText) findViewById(R.id.edt_religion);
            religion = editText.getText().toString();
        }
    }

    private void listPregnancy() {
        mPregnancy.clear();
        for (int i = 1; i <= 10; i++) {
            mPregnancy.add(i);
        }

        ArrayAdapter<Integer> adapterProvince = new ArrayAdapter<Integer>(this,
                android.R.layout.simple_dropdown_item_1line, mPregnancy);
        spinnerPregnancy.setAdapter(adapterProvince);
    }

    private void onCreateBirthRecord(int size){

        recyclerView(size);
    }

    private List<indexBirthRecord> initIndex(int size) {

        dataset.clear();
        for (int i = 0; i < size; i++) {
            indexBirthRecord indexBirthRecord = new indexBirthRecord(i);
            dataset.add(indexBirthRecord);
        }
        birthRecords = new String[size];

        return dataset;
    }


    private void recyclerView(int size) {
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        TextView tvFirst = (TextView) findViewById(R.id.birth_record_first);


        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new IndexBirthRecordAdapter(this, initIndex(size));
        mRecyclerView.setAdapter(mAdapter);

        if(size == 1){
            mRecyclerView.setVisibility(View.GONE);
            tvFirst.setVisibility(View.VISIBLE);
            birthRecords[0] = "ครรภ์แรก";
        }else {
            mRecyclerView.setVisibility(View.VISIBLE);
            tvFirst.setVisibility(View.GONE);
        }

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this,
                mRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
            }

            @Override
            public void onItemLongClick(View view, int position) {
            }
        }));
    }

    /**
     * Get data in array form adapter
     */
    private void getBirthRecord() {
        birthRecord = "";
        for (int i = 0; i < birthRecords.length; i++) {
            birthRecord += birthRecords[i] + ",";
        }
        birthRecord = birthRecord.substring(0, birthRecord.length() - 1);
    }

    private void otherReligionShow() {
        try {
            RadioGroup radioGroup = (RadioGroup) findViewById(R.id.religion);
            int radioButtonID = radioGroup.getCheckedRadioButtonId();
            RadioButton radioButton = (RadioButton) radioGroup.findViewById(radioButtonID);
            radioButton.setChecked(false);
        } catch (Exception e) {
        }
        RadioButton radioButtonOther = (RadioButton) findViewById(R.id.rbt_religion_other);
        radioButtonOther.setChecked(true);
        TextInputLayout textInputLayout = (TextInputLayout) findViewById(R.id.input_religion);
        textInputLayout.setVisibility(View.VISIBLE);
    }

    private void otherReligionHide() {
        try {

            RadioButton radioButtonOther = (RadioButton) findViewById(R.id.rbt_religion_other);
            radioButtonOther.setChecked(false);

            TextInputLayout textInputLayout = (TextInputLayout) findViewById(R.id.input_religion);
            textInputLayout.setVisibility(View.GONE);
        } catch (Exception e) {

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_PHOTO_FOR_AVATAR && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                return;
            }

            try {

                InputStream inputStream = getContentResolver().openInputStream(data.getData());
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);

                Drawable drawable = new BitmapDrawable(getResources(), bitmap);
                Log.d(TAG, String.valueOf(inputStream));


                imageView.setImageDrawable(drawable);


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private void saveEditDone() {
        if (isValidate()) {


            String UID = FirebaseAuth.getInstance().getCurrentUser().getUid();
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            final DatabaseReference myRef = database.getReference(UID).child("information");

            myRef.child("name").setValue(name);
            myRef.child("lastname").setValue(lastName);
            myRef.child("birthdate").setValue(birthDate);
            myRef.child("tel").setValue(tel);
            myRef.child("address").child("province").setValue(changwats);
            myRef.child("address").child("distinct").setValue(amphoes);
            myRef.child("address").child("sub distinct").setValue(tambons);
            myRef.child("sickness").setValue(sickness);
            myRef.child("congenital").setValue(congenital);
            myRef.child("pregnancy").setValue(pregnancy);
            myRef.child("gestational").setValue(gestational);
            myRef.child("religion").setValue(religion);
            myRef.child("salary").setValue(salary);
            myRef.child("education").setValue(education);
            myRef.child("job").setValue(job);
            myRef.child("birthRecord").setValue(birthRecord);
            myRef.child("pregnancy").setValue(pregnancy);

            StorageReference mStorageRef = FirebaseStorage.getInstance().getReference();
            StorageReference imageRef = mStorageRef.child(UID + "/profile.JPEG");

            imageView.buildDrawingCache();
            Bitmap bitmap = imageView.getDrawingCache();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
            byte[] task = baos.toByteArray();

            UploadTask mUploadTask = imageRef.putBytes(task);


            imageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    String url = uri.toString();
                    myRef.child("image").setValue(url);
                    Log.d("url", url);

                    Intent intent = new Intent(getApplicationContext(), PersonalActivity.class);
//                    intent.putExtra("url", url);
                    startActivity(intent);
                    hideProgressDialog();
                }
            });


        }
    }

    private boolean isValidate() {
        showProgressDialog();

        final EditText edtName = (EditText) findViewById(R.id.edt_name);
        final EditText edtLastName = (EditText) findViewById(R.id.edt_lastname);
        final EditText edtBirthdate = (EditText) findViewById(R.id.edt_date);
        final EditText edtTel = (EditText) findViewById(R.id.edt_tel);
        final EditText edtSickness = (EditText) findViewById(R.id.edt_sickness);
        final EditText edtGestational = (EditText) findViewById(R.id.edt_gestational_age);
        final EditText edtPregnancy = (EditText) findViewById(R.id.edt_pregnancy);
        final EditText edtSalary = (EditText) findViewById(R.id.edt_salary);
        final EditText edtBirthRecord = (EditText) findViewById(R.id.edt_birth_record);


        name = edtName.getText().toString();
        lastName = edtLastName.getText().toString();
        birthDate = edtBirthdate.getText().toString();
        tel = edtTel.getText().toString();
        sickness = edtSickness.getText().toString();
        gestational = edtGestational.getText().toString();
        salary = edtSalary.getText().toString();


        if (isCongenital) {
            congenital = "";
            getCongenital();
            congenital = congenital.substring(0, congenital.length() - 2);
        }
        if (isSickness) {
            getSickness();
        }
        if (isPregnancy) {
            getBirthRecord();
        }else{
            pregnancy = edtPregnancy.getText().toString();
            birthRecord = edtBirthRecord.getText().toString();
        }
        getReligion();


        if (name.isEmpty() ||
                lastName.isEmpty() ||
                tel.isEmpty() ||
                birthDate.isEmpty()) {

            Toast.makeText(getApplicationContext(), "All fields are required.",
                    Toast.LENGTH_SHORT).show();
            hideProgressDialog();
            return false;
        }
        return true;
    }

    private void datePicker(int viewId) {
        DialogFragment newFragment = new DatePickerFragment(getApplicationContext(), activity, viewId);
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    private void upDateUI() {

        showProgressDialog();

        String UID = FirebaseAuth.getInstance().getCurrentUser().getUid();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(UID).child("information");

        final EditText edtId = (EditText) findViewById(R.id.edt_id);
        final EditText edtName = (EditText) findViewById(R.id.edt_name);
        final EditText edtLastName = (EditText) findViewById(R.id.edt_lastname);
        final EditText edtBirthdate = (EditText) findViewById(R.id.edt_date);
        final EditText edtTel = (EditText) findViewById(R.id.edt_tel);
        final EditText edtAddress = (EditText) findViewById(R.id.edt_address);
        final EditText edtSickness = (EditText) findViewById(R.id.edt_sickness);
        final EditText edtCongenital = (EditText) findViewById(R.id.edt_congenital_disorder);
        final EditText edtGestational = (EditText) findViewById(R.id.edt_gestational_age);
        final EditText edtPregnancy = (EditText) findViewById(R.id.edt_pregnancy);
        final EditText edtEmail = (EditText) findViewById(R.id.edt_email);
        final EditText edtLine = (EditText) findViewById(R.id.edt_line);
        final EditText edtBirthRecord = (EditText) findViewById(R.id.edt_birth_record);
        final EditText edtSalary = (EditText) findViewById(R.id.edt_salary);

        final RadioButton radioButton1 = (RadioButton) findViewById(R.id.rbt_religion_1);
        final RadioButton radioButton2 = (RadioButton) findViewById(R.id.rbt_religion_2);
        final RadioButton radioButton3 = (RadioButton) findViewById(R.id.rbt_religion_3);
        final RadioButton radioButtonOther = (RadioButton) findViewById(R.id.rbt_religion_other);
        final EditText edtReligion = (EditText) findViewById(R.id.edt_religion);
        final TextInputLayout inputReligion = (TextInputLayout) findViewById(R.id.input_religion);


        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String setId = dataSnapshot.child("id").getValue(String.class);
                String setName = dataSnapshot.child("name").getValue(String.class);
                String setLastName = dataSnapshot.child("lastname").getValue(String.class);
                String setProvince = dataSnapshot.child("address").child("province").getValue(String.class);
                String setDistinct = dataSnapshot.child("address").child("distinct").getValue(String.class);
                String setSubDistinct = dataSnapshot.child("address").child("sub distinct").getValue(String.class);
                String setBirthDate = dataSnapshot.child("birthdate").getValue(String.class);
                String setTel = dataSnapshot.child("tel").getValue(String.class);
                String setPregnancy = dataSnapshot.child("pregnancy").getValue(String.class);
                String setSickness = dataSnapshot.child("sickness").getValue(String.class);
                String setGestational = dataSnapshot.child("gestational").getValue(String.class);
                String setCongenital = dataSnapshot.child("congenital").getValue(String.class);
                String setEmail = dataSnapshot.child("email").getValue(String.class);
                String setLine = dataSnapshot.child("line").getValue(String.class);
                String setBirthRecord = dataSnapshot.child("birthRecord").getValue(String.class);
                String uri = dataSnapshot.child("image").getValue(String.class);
                String setReligion = dataSnapshot.child("religion").getValue(String.class);
                String setSalary = dataSnapshot.child("salary").getValue(String.class);
                String setEducation = dataSnapshot.child("education").getValue(String.class);
                String setJob = dataSnapshot.child("job").getValue(String.class);

                if (uri != null) {

                    Glide.with(getApplicationContext())
                            .load(uri)
                            .fitCenter()
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(imageView);
                }

                edtId.setText(setId);
                edtName.setText(setName);
                edtLastName.setText(setLastName);
                edtBirthdate.setText(setBirthDate);
                edtTel.setText(setTel);
                edtAddress.setText("ต." + setSubDistinct + " อ." + setDistinct + " จ." + setProvince);
                edtCongenital.setText(setCongenital);
                edtGestational.setText(setGestational);
                edtPregnancy.setText(setPregnancy);
                edtSickness.setText(setSickness);
                edtBirthRecord.setText(setBirthRecord);
                edtEmail.setText(setEmail);
                edtLine.setText(setLine);
                edtSalary.setText(setSalary);

                if (setReligion == null) {
                    setReligion = "";
                }

                switch (setReligion) {
                    case "พุทธ":
                        radioButton1.setChecked(true);
                        break;
                    case "คริสต์":
                        radioButton2.setChecked(true);
                        break;
                    case "อิสลาม":
                        radioButton3.setChecked(true);
                        break;

                    default:
                        radioButtonOther.setChecked(true);
                        inputReligion.setVisibility(View.VISIBLE);
                        edtReligion.setText(setReligion);
                        break;
                }

                changwats = setProvince;
                amphoes = setDistinct;
                tambons = setSubDistinct;

                id = edtId.getText().toString();
                name = edtName.getText().toString();
                lastName = edtLastName.getText().toString();
                birthDate = edtBirthdate.getText().toString();
                tel = edtTel.getText().toString();

                congenital = setCongenital;
                sickness = setSickness;

                mEducation.add(setEducation);
                mJob.add(setJob);

                listEducation();
                listJob();

                hideProgressDialog();
            }

            @Override
            public void onCancelled(DatabaseError error) {
            }
        });

    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        switch (group.getId()) {
            case R.id.religion:
                otherReligionHide();
                break;
        }
    }
}
