package asanee.tosaganjana.kku.ac.th.labour.pain;


import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import asanee.tosaganjana.kku.ac.th.labour.R;
import asanee.tosaganjana.kku.ac.th.labour.data.DataDetailActivity;
import asanee.tosaganjana.kku.ac.th.labour.list.index.IndexAdapter;
import asanee.tosaganjana.kku.ac.th.labour.list.index.IndexAdminAdapter;
import asanee.tosaganjana.kku.ac.th.labour.list.index.RecyclerItemClickListener;
import asanee.tosaganjana.kku.ac.th.labour.list.index.index;
import asanee.tosaganjana.kku.ac.th.labour.list.index.indexAdmin;
import asanee.tosaganjana.kku.ac.th.labour.main.BaseActivity;
import asanee.tosaganjana.kku.ac.th.labour.main.MainActivity;
import asanee.tosaganjana.kku.ac.th.labour.personal.PersonalActivity;
import asanee.tosaganjana.kku.ac.th.labour.personal.admin.AdminActivity;

public class PainActivity extends BaseActivity implements View.OnClickListener{

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    List<index> dataset = new ArrayList<index>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pain);

        init();
        recyclerView();

    }

    private void init(){
        findViewById(R.id.back).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back:
                goBack(MainActivity.class);
                break;
        }
    }

    private List<index> initIndex() {

        index index1 = new index(1,"การเจ็บครรภ์",getString(R.string.text_pain_1));
        dataset.add(index1);

        index index2 = new index(2,"ข้อควรปฏิบัติ",getString(R.string.text_pain_2));
        dataset.add(index2);

        return dataset;
    }

    @Override
    public void onBackPressed() {  goBack(MainActivity.class); }


    private void recyclerView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);


        mLayoutManager = new LinearLayoutManager(PainActivity.this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new IndexAdapter(PainActivity.this, initIndex());
        mRecyclerView.setAdapter(mAdapter);


        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this,
                mRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                goToData(DataDetailActivity.class,dataset.get(position).getTitle(),
                        dataset.get(position).getDetail());
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));
    }

}
