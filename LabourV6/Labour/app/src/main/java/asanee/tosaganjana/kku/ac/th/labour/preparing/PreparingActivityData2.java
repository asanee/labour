package asanee.tosaganjana.kku.ac.th.labour.preparing;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import asanee.tosaganjana.kku.ac.th.labour.R;
import asanee.tosaganjana.kku.ac.th.labour.main.BaseActivity;
import asanee.tosaganjana.kku.ac.th.labour.main.MainActivity;

public class PreparingActivityData2 extends BaseActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preparing_data2);

        init();
        updateView();
    }

    private void init(){

        findViewById(R.id.back).setOnClickListener(this);
        findViewById(R.id.home).setOnClickListener(this);
    }

    private void updateView() {
        TextView tvData1 = (TextView) findViewById(R.id.data1);
        TextView tvData2 = (TextView) findViewById(R.id.data2);
        TextView tvData3 = (TextView) findViewById(R.id.data3);

        tvData1.setText(Html.fromHtml(getString(R.string.text_preparing_2_data_1)));
        tvData2.setText(Html.fromHtml(getString(R.string.text_preparing_2_data_2)));
        tvData3.setText(Html.fromHtml(getString(R.string.text_preparing_2_data_3)));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back:
                super.onBackPressed();
                break;
            case R.id.home:
                goBack(MainActivity.class);
                break;
        }

    }
}
