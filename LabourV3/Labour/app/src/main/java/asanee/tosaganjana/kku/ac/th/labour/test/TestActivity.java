package asanee.tosaganjana.kku.ac.th.labour.test;

import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.Scanner;

import asanee.tosaganjana.kku.ac.th.labour.R;
import asanee.tosaganjana.kku.ac.th.labour.main.MainActivity;

public class TestActivity extends AppCompatActivity implements View.OnClickListener{

    private StringBuilder builder;
    private Button submit,next;
    private TextView problem;
    private int score = 0,count = 0;
    private String choice3;
    private RadioGroup group;
    private RadioButton radio1,radio2,radio3;
    private JSONArray exam;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        setTitle("แบบประเมิน");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        problem = (TextView) findViewById(R.id.problem);
        group = (RadioGroup) findViewById(R.id.radio_group);
        radio1 = (RadioButton) findViewById(R.id.radio1);
        radio2 = (RadioButton) findViewById(R.id.radio2);
        radio3 = (RadioButton) findViewById(R.id.radio3);

        submit = (Button) findViewById(R.id.submit);
        next = (Button) findViewById(R.id.next);


        submit.setOnClickListener(this);

        Resources res = getResources();
        InputStream is = res.openRawResource(R.raw.test);
        Scanner scanner = new Scanner(is);

        builder = new StringBuilder();

        while (scanner.hasNextLine()) {
            builder.append(scanner.nextLine());
        }

        readFile(builder.toString(),count);


    }

    public void readFile(String s,int i){
        try {
            group.clearCheck();
            JSONObject root = new JSONObject(s);
            exam = (JSONArray) root.get("exam");

            String question = exam.getJSONObject(i).getString("problem" + String.valueOf(i+1));
            String choice1 = exam.getJSONObject(i).getString("choice1");
            String choice2 = exam.getJSONObject(i).getString("choice2");
            if(i <= 3) {
                submit.setVisibility(View.INVISIBLE);
                choice3 = exam.getJSONObject(i).getString("choice3");
                radio3.setText("C. " + choice3);
            }else{
               /* if(count == exam.length()) {
                    Intent I = new Intent(MainActivity.this,Main2Activity.class);
                    I.putExtra("score",score);
                    startActivity(I);
                    submit.setVisibility(View.VISIBLE);
                }else {
                    submit.setVisibility(View.INVISIBLE);
                }*/
                submit.setVisibility(View.INVISIBLE);
                radio3.setVisibility(View.INVISIBLE);
            }

            problem.setText(question);
            radio1.setText("A. " + choice1);
            radio2.setText("B. " + choice2);

        }catch (JSONException e){

        }

    }

    //num 1 - 4
    public void checkAns1(int i){
        if(i == 1){
            score += 10;
        }else if(i == 2){
            score += 2;
        }else if(i == 3){
            score += 0;
        }
    }

    //num 4 - last num
    public void checkAns2(int i){

        if(i == 1){
            if(count+1 == exam.length()){
                radio1.setChecked(true);
                radio2.setEnabled(false);
            }else {
            }
            score += 10;

        }else if(i == 2){
            if(count+1 == exam.length()) {
                radio2.setChecked(true);
                radio1.setEnabled(false);
            }else {
            }
            score += 0;
        }
    }

    @Override
    public void onClick(View view) {
        if (view == submit) {
            Intent I = new Intent(TestActivity.this, ResultTest.class);
            I.putExtra("score", score);
            startActivity(I);
        } else if (view == next) {
            if (radio1.isChecked() || radio2.isChecked() || radio3.isChecked()) {
                if (count <= 3) {   //check number 1 -4
                    if (radio1.isChecked()) {
                        checkAns1(1);
                    } else if (radio2.isChecked()) {
                        checkAns1(2);
                    } else {
                        checkAns1(3);
                    }
                    count++;
                    readFile(builder.toString(), count);

                } else { // check number 4 - last num

                    if (radio1.isChecked()) {
                        checkAns2(1);
                    } else if (radio2.isChecked()) {
                        checkAns2(2);
                    }

                    count++;
                    if (count == exam.length()) { // last number will show button submit and hide button next

                        submit.setVisibility(View.VISIBLE);
                        next.setVisibility(View.INVISIBLE);
                    } else {
                        readFile(builder.toString(), count);
                    }
                }
            }else
                Toast.makeText(TestActivity.this, "Please select answer", Toast.LENGTH_LONG).show();
        }
    }
}