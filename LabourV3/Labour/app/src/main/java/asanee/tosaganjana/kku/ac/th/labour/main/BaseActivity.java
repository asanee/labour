package asanee.tosaganjana.kku.ac.th.labour.main;

/**
 * Created by Puppymind on 29/4/2560.
 */
import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;

import asanee.tosaganjana.kku.ac.th.labour.R;

public class BaseActivity extends AppCompatActivity {
    private ProgressDialog mProgressDialog;

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("loading...");
            mProgressDialog.setIndeterminate(true);
        }
        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        hideProgressDialog();
    }
}