package asanee.tosaganjana.kku.ac.th.labour.list.index;

/**
 * Created by Puppymind on 29/4/2560.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.StringSignature;

import java.util.List;
import java.util.UUID;

import asanee.tosaganjana.kku.ac.th.labour.R;
import asanee.tosaganjana.kku.ac.th.labour.fonts.CustomTextView;
import asanee.tosaganjana.kku.ac.th.labour.personal.PersonalActivity;
import de.hdodenhof.circleimageview.CircleImageView;

public class IndexAdminAdapter extends RecyclerView.Adapter<IndexAdminAdapter.ViewHolder> {

    private List<indexAdmin> mIndices;
    private Context mContext;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public CustomTextView mName;
        public CustomTextView mId;
        public CircleImageView imageView;

        public ViewHolder(View view) {
            super(view);

            mName = (CustomTextView) view.findViewById(R.id.admin_name);
            mId = (CustomTextView) view.findViewById(R.id.admin_id);
            imageView = (CircleImageView) view.findViewById(R.id.image_circle);
        }
    }

    public IndexAdminAdapter(Context context, List<indexAdmin> dataset) {
        mIndices = dataset;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.recycler_view_admin, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        indexAdmin index = mIndices.get(position);

        viewHolder.mName.setText(index.getName());
        viewHolder.mId.setText(index.getId());

        if (!index.getUrl().isEmpty()) {
            Log.d("url", index.getUrl());

            Glide.with(mContext)
                    .load(index.getUrl())
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(viewHolder.imageView);
        }
    }

    @Override
    public int getItemCount() {
        return mIndices.size();
    }
}