package asanee.tosaganjana.kku.ac.th.labour.test;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import asanee.tosaganjana.kku.ac.th.labour.R;

public class ResultPreTest extends AppCompatActivity {

    private TextView text_score;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_pre_test);

        text_score = (TextView) findViewById(R.id.text_score);

        Bundle bundle = getIntent().getExtras();
        int score = bundle.getInt("score");

        text_score.setText("Your score is = " + String.valueOf(score));
    }
}
