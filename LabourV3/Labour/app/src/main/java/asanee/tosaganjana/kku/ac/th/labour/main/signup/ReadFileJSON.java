package asanee.tosaganjana.kku.ac.th.labour.main.signup;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import asanee.tosaganjana.kku.ac.th.labour.R;
import asanee.tosaganjana.kku.ac.th.labour.main.BaseActivity;

/**
 * Created by Puppymind on 20/5/2560.
 */

public class ReadFileJSON extends BaseActivity {

    private static final String SELECT = "-- pleases select --";
    Context context;

    public ReadFileJSON(Context context){
        this.context = context;
    }

    public String getProvince(String address, ArrayList list) {

        ByteArrayOutputStream byteArrayOutputStream = readFile(R.raw.changwats);
        try {
            list.add(SELECT);
            JSONObject jObject = new JSONObject(
                    byteArrayOutputStream.toString());
            JSONObject results = (JSONObject) jObject.get("th");

            JSONArray changwats = (JSONArray) results.get("changwats");
            for (int i = 0; i < changwats.length(); i++) {
                JSONObject item = (JSONObject) changwats.get(i);
                String name = (String) item.get("name");
                if (address.equals(name)) {
                    String changwatsPid = (String) item.get("pid");
                    return changwatsPid;
                }
                Log.d("json", name);
                list.add(name);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    public String getDistinct(String address, ArrayList list, String changwatsPid) {

        ByteArrayOutputStream byteArrayOutputStream = readFile(R.raw.amphoes);

        try {
            list.add(SELECT);
            JSONObject jObject = new JSONObject(
                    byteArrayOutputStream.toString());
            JSONObject results = (JSONObject) jObject.get("th");
            JSONArray jsonArray = (JSONArray) results.get("amphoes");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject item = (JSONObject) jsonArray.get(i);
                String pid = (String) item.get("changwat_pid");
                if (pid.equals(changwatsPid)) {
                    String name = (String) item.get("name");
                    if (name.equals(address)) {
                        String amphoesPid = (String) item.get("pid");
                        return amphoesPid;
                    }
                    list.add(name);

                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void getSubDistinct(ArrayList list, String amphoesPid) {

        ByteArrayOutputStream byteArrayOutputStream = readFile(R.raw.tambons);
        try {
            list.add(SELECT);
            JSONObject jObject = new JSONObject(
                    byteArrayOutputStream.toString());
            JSONObject results = (JSONObject) jObject.get("th");

            JSONArray jsonArray = (JSONArray) results.get("tambons");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject item = (JSONObject) jsonArray.get(i);
                String pid = (String) item.get("amphoe_pid");
                if (pid.equals(amphoesPid)) {
                    String name = (String) item.get("name");
                    list.add(name);

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private ByteArrayOutputStream readFile(int raw) {
        InputStream inputStream = context.getResources().openRawResource(raw);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        int index;
        try {
            index = inputStream.read();
            while (index != -1) {
                byteArrayOutputStream.write(index);
                index = inputStream.read();
            }
            inputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return byteArrayOutputStream;
    }

    public void sort(ArrayList list) {
        Collections.sort(list, new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return s1.compareToIgnoreCase(s2);
            }
        });
    }
}
