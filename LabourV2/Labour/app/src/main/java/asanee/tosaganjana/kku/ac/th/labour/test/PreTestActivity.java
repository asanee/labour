package asanee.tosaganjana.kku.ac.th.labour.test;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import asanee.tosaganjana.kku.ac.th.labour.R;

public class PreTestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_test);
    }
}
