package asanee.tosaganjana.kku.ac.th.labour.list.index;

/**
 * Created by Puppymind on 29/4/2560.
 */

public class indexAdmin {
    private String name;
    private String id;
    private String url;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public indexAdmin(String name, String id, String url) {

        this.name = name;
        this.id = id;
        this.url = url;
    }
}