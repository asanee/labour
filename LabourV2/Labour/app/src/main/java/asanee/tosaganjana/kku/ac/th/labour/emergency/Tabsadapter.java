package asanee.tosaganjana.kku.ac.th.labour.emergency;

/**
 * Created by Puppymind on 29/4/2560.
 */

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import asanee.tosaganjana.kku.ac.th.labour.pain.IndexPainFragment;
import asanee.tosaganjana.kku.ac.th.labour.pain.VideoPainFragment;

public class Tabsadapter extends FragmentStatePagerAdapter{

    private int TOTAL_TABS = 2;

    public Tabsadapter(FragmentManager fm) {
        super(fm);
        // TODO Auto-generated constructor stub
    }

    @Override
    public Fragment getItem(int index) {
        // TODO Auto-generated method stub
        switch (index) {
            case 0:
                return new IndexEmergencyFragment();

            case 1:
                return new VideoEmergencyFragment();

        }

        return null;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return TOTAL_TABS;
    }

}