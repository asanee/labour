package asanee.tosaganjana.kku.ac.th.labour.personal.admin;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import asanee.tosaganjana.kku.ac.th.labour.R;
import asanee.tosaganjana.kku.ac.th.labour.fonts.CustomTypefaceSpan;
import asanee.tosaganjana.kku.ac.th.labour.list.index.IndexAdapter;
import asanee.tosaganjana.kku.ac.th.labour.list.index.IndexAdminAdapter;
import asanee.tosaganjana.kku.ac.th.labour.list.index.RecyclerItemClickListener;
import asanee.tosaganjana.kku.ac.th.labour.list.index.index;
import asanee.tosaganjana.kku.ac.th.labour.list.index.indexAdmin;
import asanee.tosaganjana.kku.ac.th.labour.main.MainActivity;
import asanee.tosaganjana.kku.ac.th.labour.main.signup.SignUpActivity;
import asanee.tosaganjana.kku.ac.th.labour.personal.PersonalActivity;
import de.hdodenhof.circleimageview.CircleImageView;

public class AdminActivity extends MainActivity implements View.OnClickListener{

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    List<indexAdmin> dataset = new ArrayList<indexAdmin>();
    ArrayList<String> item = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        setTitle("ADMIN");

        upDateUI();
        recyclerView();

        initDrawerLayout(null);


        findViewById(R.id.back).setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        Intent setIntent = new Intent(this, MainActivity.class);
        startActivity(setIntent);
    }


    private List<indexAdmin> initIndex() {

        return dataset;
    }

    private void upDateUI() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference();

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()) {
//                    Log.d("child", child.getKey());
                    String key = child.getKey();
                    String master = dataSnapshot.child(key).child("master").getValue(String.class);

                    if (master == null) {
                        DataSnapshot information = dataSnapshot.child(key).child("information");

                        String url = information.child("image").getValue(String.class);
                        String name = information.child("name").getValue(String.class);
                        String lastName = information.child("lastname").getValue(String.class);
                        String id = information.child("id").getValue(String.class);

                        if (url == null || name == null || lastName == null || id == null) {

                        } else {
                            indexAdmin index = new indexAdmin(name + " " + lastName, id, url);
                            dataset.add(index);
                            item.add(key);
                        }
                    }else {
                        Log.d("master",master);
                    }
                }

                mLayoutManager = new LinearLayoutManager(AdminActivity.this);
                mRecyclerView.setLayoutManager(mLayoutManager);

                mAdapter = new IndexAdminAdapter(AdminActivity.this, initIndex());
                mRecyclerView.setAdapter(mAdapter);
            }

            @Override
            public void onCancelled(DatabaseError error) {
            }
        });
    }

    private void recyclerView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this,
                mRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                Intent intent = new Intent(getApplicationContext(), PersonalActivity.class);
                intent.putExtra("key", item.get(position));
                startActivity(intent);
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));
    }

}
