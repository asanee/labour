package asanee.tosaganjana.kku.ac.th.labour.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by Puppymind on 30/4/2560.
 */

public class CustomButton extends android.support.v7.widget.AppCompatButton {

    public CustomButton(Context context) {
        super(context);
        initTypFace();
    }
    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        initTypFace();
    }

    public CustomButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initTypFace();
    }

    private void initTypFace() {
        Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), "THSarabun.ttf");
        setTypeface(typeface);
    }
}
